use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq, JsonSchema)]
pub struct SiteMapImages {
    pub site_maps: Vec<SiteMapImage>,
}

#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
)]
pub struct SiteMapImage {
    /// This id is the same as site_id in the world.
    pub id: i32,
    #[serde(skip)]
    pub data: Vec<u8>,
    pub format: String,
}

impl SiteMapImages {
    /// Create a new site map image collection
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for SiteMapImages {
    fn example() -> Self {
        Self::default()
    }
}

impl SiteMapImage {
    /// Create a new site map image
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for SiteMapImage {
    fn example() -> Self {
        Self::default()
    }
}

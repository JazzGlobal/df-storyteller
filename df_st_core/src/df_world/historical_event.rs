use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// An historical event that happened at some point in the world.
/// This is (usually are large) list of things that happened.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct HistoricalEvent {
    /// Identifier for the historical event.
    /// `id` must be unique for the whole world.
    pub id: i32,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub year: Option<i32>, // TODO Date
    pub seconds72: Option<i32>,

    // All others are optional
    // legends
    /// The attacking squad in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub a_hf_ids: Vec<i32>,
    /// The attacking leader in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub a_leader_hf_id: Option<i32>,
    /// The attacking leaders role? in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub a_leadership_roll: Option<i32>,
    /// The attacking squad in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub a_squad_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub a_support_merc_en_id: Option<i32>,
    /// The attacking side tactician (commander) in tactical situations
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub a_tactician_hf_id: Option<i32>,
    /// Used in tactical situations
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub a_tactics_roll: Option<i32>,
    /// In a `site died` type, define if the site is abandoned afterwards.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub abandoned: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub account_shift: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub acquirer_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub acquirer_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub action: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub actor_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub agreement_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub allotment: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub allotment_index: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ally_defense_bonus: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub appointer_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub arresting_en_id: Option<i32>,
    /// In a `masterpiece_created_item_improvement` type, id to `art_image_chunk` (not in legends).
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub art_id: Option<i32>,
    /// In a `masterpiece_created_item_improvement` type,
    /// id to `art_image` (together with `art_id`) (not in legends).
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub art_sub_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub artifact_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub attacker_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub attacker_general_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub attacker_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub attacker_merc_en_id: Option<i32>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub abuse_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub anon_3: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub anon_4: Option<i32>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub body_state: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub builder_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub building_profile_id: Option<i32>,
    // legends_plus
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub bodies_hf_id: Vec<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub body_part: Option<i32>,
    /// In a `masterpiece_created_arch_construct` type, ??.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub building_custom: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub building_type: Option<String>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub building_subtype: Option<String>, // TODO check type

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cause: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub changee_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub changer_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub circumstance: Option<String>, // some kind of type "historical event collection"
    #[serde(skip_serializing_if = "Option::is_none")]
    pub circumstance_id: Option<i32>, // ID for that type "565" -> `historical_event_collection_id`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub civ_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub claim: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub coconspirator_bonus: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub coconspirator_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub competitor_hf_id: Vec<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub confessed_after_apb_arrest_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub conspirator_hf_id: Vec<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub contact_hf_id: Option<i32>,
    /// In a `first contact` type event, the side that got contacted (not initiator).
    /// See `contactor_en_id` for the initiator.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub contacted_en_id: Option<i32>,
    /// In a `first contact` type event, the side that initiated the contact.
    /// See `contacted_en_id` for the side that was contacted.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub contactor_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub convict_is_contact: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub convicted_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub convicter_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub coord: Option<String>, // Change to Coordinate, depends on legends.xml
    #[serde(skip_serializing_if = "Option::is_none")]
    pub corrupt_convicter_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub corruptor_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub corruptor_identity: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub corruptor_seen_as: Option<String>,
    /// In a `masterpiece_lost` type,
    /// the historical event when the masterpiece was created.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creation_he_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub crime: Option<String>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub caste: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub circumstance_obj: Option<HECircumstance>, // similar to `circumstance`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator_unit_id: Option<i32>, // Value = -1

    // legends
    /// The ?? on the defending side in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_effect: Option<i32>,
    /// The defending figures of the squad in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub d_hf_ids: Vec<i32>,
    /// The ?? on the defending side in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_interaction: Option<i32>,
    /// The ?? on the defending side in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_number: Option<i32>,
    /// The ?? on the defending side in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_race: Option<i32>,
    /// The amount?? of killed members on the defending side in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_slain: Option<i32>,
    /// The defending squad in squad vs squad
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_squad_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_support_merc_en_id: Option<i32>,
    /// The defending side tactician (commander) in tactical situations
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_tactician_hf_id: Option<i32>,
    /// Used in tactical situations
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub d_tactics_roll: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub death_penalty: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub defender_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub defender_general_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub defender_merc_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub delegated: Option<bool>,
    /// The depot of entity where the merchant traded with?
    /// Used in merchant events.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub depot_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dest_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dest_site_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dest_structure_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub destroyed_structure_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub destroyer_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub detected: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub did_not_reveal_all_in_interrogation: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub disturbance: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dispute: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub doer_hf_id: Option<i32>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub death_cause: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub destination: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub doer: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dye_mat: Option<String>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dye_mat_index: Option<i32>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dye_mat_type: Option<i32>, // TODO check type

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub enslaved_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entity_id_1: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entity_id_2: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub exiled: Option<bool>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub expelled_creature: Vec<i32>, // What does this link to?
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub expelled_hf_id: Vec<i32>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub expelled_number: Vec<i32>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub expelled_pop_id: Vec<i32>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub eater_hf_id: Option<i32>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub failed_judgment_test: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub feature_layer_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub first: Option<bool>,
    /// In a `entity fled site` type event, the civilization id that is going away??
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fled_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fooled_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub form_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub framer_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from_original: Option<bool>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub gambler_hf_id: Option<i32>,
    /// The name of the ghost? type of ghost?
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ghost: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub giver_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub giver_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub group_1_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub group_2_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub group_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub groups_hf_id: Vec<i32>,

    /// In a `merchant` type event, ??
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hardship: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub held_firm_in_interrogation: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hf_rep_1_of_2: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hf_rep_2_of_1: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hf_id1: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hf_id2: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hf_id_target: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub honor_id: Option<i32>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identity_id1: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identity_id2: Option<i32>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub implicated_hf_id: Vec<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inherited: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub initiating_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub instigator_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interaction: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interrogator_hf_id: Option<i32>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identity_caste: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identity_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identity_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identity_nemesis_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identity_race: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub imp_mat: Option<String>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub imp_mat_index: Option<i32>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub imp_mat_type: Option<i32>, // TODO check type
    /// The subtype of the item that was improved
    /// in `masterpiece_created_item_improvement` type event
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub improvement_subtype: Option<String>,
    /// Updated in v0.2.0 from `i32` to `String`
    /// ( Updated in "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub improvement_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub injury_type: Option<String>,
    // pub interaction_id: Option<i32>, // Not used at the moment
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interaction_action: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interaction_string: Option<String>,
    /// The id of the item? Same as `item`?
    /// ID seems to large to refer to artifact id.
    /// Occurs in `masterpiece_created_item` type events
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub item_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub item_mat: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub item_subtype: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub item_type: Option<String>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub join_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub joined_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub joiner_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub joining_en_id: Vec<i32>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub knowledge: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub last_owner_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub law_add: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub law_remove: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub leader_hf_id: Option<i32>,
    /// A civilization leaving a site in "spotted leaving site"
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub leaver_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub link: Option<String>,
    /// In a `merchant` type event, define if the merchant lost valuables?
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lost_value: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lure_hf_id: Option<i32>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub link_type: Option<String>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub master_wc_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub method: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modification: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mood: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub moved_to_site_id: Option<i32>,
    // legends_plus
    /// In a `masterpiece_created_arch_construct` type event, define the maker of the masterpiece.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub maker_hf_id: Option<i32>,
    /// In a `masterpiece_created_arch_construct` type event,
    /// define the entity of the maker that created the masterpiece.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub maker_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mat: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mat_type: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mat_index: Option<i32>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub name_only: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_ab_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_account: Option<i32>,
    /// In a `artifact transformed` type event, the id of the new artifact.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_artifact_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_caste: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_equipment_level: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_leader_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_race_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_site_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_job: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_structure_id: Option<i32>,
    /// In a `plundered site` type event ??.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub no_defeat_mention: Option<bool>,
    /// On a `hf convicted` type event it tells us that there is no
    /// prison empty for the new criminal.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub no_prison_available: Option<bool>,

    /// Used by `hf_relationship` and others.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub occasion_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old_ab_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old_account: Option<i32>,
    /// In a `artifact transformed` type event, the id of the old artifact.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old_artifact_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old_caste: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old_job: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old_race_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old_structure_id: Option<i32>,
    /// In a `insurrection started` type event, the outcome of the insurrection.
    /// Example value: "population gone"
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outcome: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub overthrown_hf_id: Option<i32>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub partial_incorporation: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub payer_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub payer_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub persecutor_en_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub persecutor_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub plotter_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pop_fl_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pop_number_moved: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pop_race: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pop_sr_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pos_taker_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub position_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub position_profile_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prison_months: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub production_zone_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub promise_to_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub property_confiscated_from_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub purchased_unowned: Option<bool>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub part_lost: Option<bool>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub pets: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pile_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub position: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub props_item_mat: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub props_item_mat_index: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub props_item_mat_type: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub props_item_subtype: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub props_item_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub props_pile_type: Option<i32>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub quality: Option<i32>,
    /// In a `entity rampaged in site` type event ??.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rampage_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ransomed_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ransomer_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reason: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reason_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rebuilt_ruined: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub receiver_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub receiver_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub relationship: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub relevant_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub relevant_id_for_method: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub relevant_position_profile_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub religion_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resident_civ_id: Option<i32>,
    /// In a `entity searched site` type event the result of the site searched.
    /// Example: "found nothing"
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub result: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "return")]
    pub return_: Option<bool>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub race_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reason_obj: Option<HEReason>, // TODO in lua. This can be an object and a value
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rebuild: Option<bool>, // See rebuilt
    #[serde(skip_serializing_if = "Option::is_none")]
    pub region_id: Option<i32>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub saboteur_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub schedule_id: Option<i32>,
    /// In a `entity searched site` type event the civilization id that searched the site.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub searcher_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub season: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub secret_goal: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub seeker_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub seller_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shrine_amount_destroyed: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub site_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub site_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub site_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub site_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub site_id_1: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub site_id_2: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub site_property_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub situation: Option<String>,
    /// The skill of the `hf_id` at the time of creating this masterpiece?
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub skill_at_time: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub slayer_caste: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub slayer_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub slayer_item_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub slayer_race: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub slayer_shooter_item_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub snatcher_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source_entity_id: Option<i32>,
    /// The `hf_id` in a `hf_relationship` type event
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source_site_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source_structure_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub speaker_hf_id: Option<i32>,
    /// A civilization leaving a site was spotted by this
    /// historical figure in "spotted leaving site".
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub spotter_hf_id: Option<i32>,
    /// Start of a tactical situations
    /// If `false` it denotes an ongoing tactical situation.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub start: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub structure_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub student_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subtype: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub successful: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub surveiled_coconspirator: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub surveiled_contact: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub surveiled_convicted: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub surveiled_target: Option<bool>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sanctify_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub secret_text: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shooter_artifact_id: Option<i32>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shooter_item: Option<String>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shooter_item_subtype: Option<String>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shooter_item_type: Option<String>, // TODO check type
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shooter_mat: Option<String>, // TODO check type (shooter_mattype)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stash_site_id: Option<i32>,

    // legends
    /// In a `insurrection started` type event,
    /// the civilization that is targeted.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_civ_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_en_id: Option<i32>,
    /// Used in `hf_relationship` event and others.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_identity: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_seen_as: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub teacher_hf_id: Option<i32>,
    /// In a `plundered site` type event, define if the attacker took items with them.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub took_items: Option<bool>,
    /// In a `plundered site` type event, define if the attacker took animals with them.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub took_livestock: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_facet: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_facet_modifier: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_facet_rating: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_relationship_factor: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_relationship_modifier: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_relationship_rating: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_value: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_value_modifier: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub top_value_rating: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub topic: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trader_entity_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trader_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trickster_hf_id: Option<i32>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub theft_method: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tree: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trickster: Option<i32>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub unit_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unit_type: Option<String>,
    /// In a `reclaim site` type event, define if the site is being unretired or never was retired.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unretire: Option<bool>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub victim: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub victim_entity: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub victim_hf_id: Option<i32>,

    // legends
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wanted_and_recognized: Option<bool>,
    /// In a `plundered site` type event, ??.
    /// ( Since = "0.2.0" )
    #[serde(skip_serializing_if = "Option::is_none")]
    pub was_raid: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub was_torture: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wc_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub winner_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub woundee_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wounder_hf_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wrongful_conviction: Option<bool>,
    // legends_plus
    #[serde(skip_serializing_if = "Option::is_none")]
    pub woundee_caste: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub woundee_race: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HECircumstance {
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub death: Option<i32>,
    pub prayer: Option<i32>,
    pub dream_about: Option<i32>,
    pub defeated: Option<i32>,
    pub murdered: Option<i32>,
    pub hec_id: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HEReason {
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub glorify_hf_id: Option<i32>,
    pub artifact_is_heirloom_of_family_hf_id: Option<i32>,
    pub artifact_is_symbol_of_entity_position: Option<i32>,
}

impl PartialEq for HECircumstance {
    fn eq(&self, other: &Self) -> bool {
        self.type_ == other.type_
    }
}

impl std::hash::Hash for HECircumstance {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.type_.hash(state);
    }
}

impl PartialEq for HEReason {
    fn eq(&self, other: &Self) -> bool {
        self.type_ == other.type_
    }
}

impl std::hash::Hash for HEReason {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.type_.hash(state);
    }
}

impl CreateNew for HistoricalEvent {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for HistoricalEvent {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for HECircumstance {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for HEReason {
    fn example() -> Self {
        Self::default()
    }
}

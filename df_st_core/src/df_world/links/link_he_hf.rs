use super::IdLink;
use crate::fillable::{Fillable, Filler};
use crate::{add_all_from_id_option, add_all_from_id_vec, DFWorld, SchemaExample};
use df_st_derive::{Fillable, Filler};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::hash::Hash;

/// An link between a `HistoricalEvent` and a `HistoricalFigure`.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
    Hash,
    PartialEq,
    Eq,
)]
pub struct LinkHEHF {
    /// A reference to a HistoricalEvent.
    pub he_id: i32,
    /// A reference to a HistoricalFigure.
    pub hf_id: i32,
}

impl IdLink for LinkHEHF {
    fn new_link(left_id: i32, right_id: i32) -> Self {
        Self {
            he_id: left_id,
            hf_id: right_id,
        }
    }
    fn get_left_id(&self) -> i32 {
        self.he_id
    }
    fn get_right_id(&self) -> i32 {
        self.hf_id
    }
    fn get_struct_name() -> &'static str {
        "LinkHEHF"
    }
    fn get_left_name() -> &'static str {
        "he_id"
    }
    fn get_right_name() -> &'static str {
        "hf_id"
    }
}

impl LinkHEHF {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn create_links(world: &DFWorld) -> HashSet<Self> {
        let mut links: HashSet<Self> = HashSet::new();

        for (_, he) in &world.historical_events {
            let he_id = he.id;
            let he_id_o = Some(he.id);
            add_all_from_id_vec!(links, he_id, he: [
                a_hf_ids,
                d_hf_ids,
                bodies_hf_id,
                competitor_hf_id,
                conspirator_hf_id,
                expelled_hf_id,
                groups_hf_id,
                implicated_hf_id,
            ]);
            add_all_from_id_option!(links, he_id_o, he: [
                a_leader_hf_id,
                a_tactician_hf_id,
                acquirer_hf_id,
                actor_hf_id,
                appointer_hf_id,
                attacker_general_hf_id,
                attacker_hf_id,
                builder_hf_id,
                changee_hf_id,
                changer_hf_id,
                coconspirator_hf_id,
                contact_hf_id,
                convicted_hf_id,
                corrupt_convicter_hf_id,
                corruptor_hf_id,
                creator_hf_id,
                d_tactician_hf_id,
                defender_general_hf_id,
                doer_hf_id,
                enslaved_hf_id,
                eater_hf_id,
                fooled_hf_id,
                framer_hf_id,
                gambler_hf_id,
                giver_hf_id,
                group_1_hf_id,
                group_2_hf_id,
                group_hf_id,
                hf_id,
                hf_id1,
                hf_id2,
                hf_id_target,
                instigator_hf_id,
                interrogator_hf_id,
                identity_hf_id,
                last_owner_hf_id,
                leader_hf_id,
                lure_hf_id,
                modifier_hf_id,
                new_leader_hf_id,
                overthrown_hf_id,
                payer_hf_id,
                persecutor_hf_id,
                plotter_hf_id,
                pos_taker_hf_id,
                promise_to_hf_id,
                property_confiscated_from_hf_id,
                ransomed_hf_id,
                ransomer_hf_id,
                receiver_hf_id,
                saboteur_hf_id,
                seeker_hf_id,
                seller_hf_id,
                site_hf_id,
                slayer_hf_id,
                snatcher_hf_id,
                speaker_hf_id,
                spotter_hf_id,
                student_hf_id,
                sanctify_hf_id,
                target_hf_id,
                teacher_hf_id,
                trader_hf_id,
                trickster_hf_id,
                victim_hf_id,
                winner_hf_id,
                woundee_hf_id,
                wounder_hf_id,
            ]);
        }
        // Check if relations correct
        let max_he_id = world.historical_events.len() as i32;
        let max_hf_id = world.historical_figures.len() as i32;
        Self::remove_incorrect(&mut links, max_he_id, max_hf_id);

        links
    }
}

impl SchemaExample for LinkHEHF {
    fn example() -> Self {
        Self::default()
    }
}

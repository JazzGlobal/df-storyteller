use super::IdLink;
use crate::fillable::{Fillable, Filler};
use crate::{add_all_from_id_option, DFWorld, SchemaExample};
use df_st_derive::{Fillable, Filler};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::hash::Hash;

/// An link between a `HistoricalEvent` and a `Site`.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
    Hash,
    PartialEq,
    Eq,
)]
pub struct LinkHESite {
    /// A reference to a HistoricalEvent.
    pub he_id: i32,
    /// A reference to a Site.
    pub site_id: i32,
}

impl IdLink for LinkHESite {
    fn new_link(left_id: i32, right_id: i32) -> Self {
        Self {
            he_id: left_id,
            site_id: right_id,
        }
    }
    fn get_left_id(&self) -> i32 {
        self.he_id
    }
    fn get_right_id(&self) -> i32 {
        self.site_id
    }
    fn get_struct_name() -> &'static str {
        "LinkHESite"
    }
    fn get_left_name() -> &'static str {
        "he_id"
    }
    fn get_right_name() -> &'static str {
        "site_id"
    }
}

impl LinkHESite {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn create_links(world: &DFWorld) -> HashSet<Self> {
        let mut links: HashSet<Self> = HashSet::new();

        for (_, he) in &world.historical_events {
            // let he_id = he.id;
            let he_id_o = Some(he.id);
            // add_all_from_id_vec!(links, he_id, he: [
            //     a_hf_ids,
            // ]);
            add_all_from_id_option!(links, he_id_o, he: [
                dest_site_id,
                moved_to_site_id,
                site_id,
                site_id_1,
                site_id_2,
                source_site_id,
                stash_site_id,
            ]);
        }
        // Check if relations correct
        let max_he_id = world.historical_events.len() as i32;
        let max_site_id = world.sites.len() as i32;
        Self::remove_incorrect(&mut links, max_he_id, max_site_id);

        links
    }
}

impl SchemaExample for LinkHESite {
    fn example() -> Self {
        Self::default()
    }
}

use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::positions::Coordinate;
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// An Mountain Peak is the position of the top of a mountain or volcano in the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct MountainPeak {
    /// Identifier for the mountain peak or volcano.
    /// `id` must be unique for the whole world.
    pub id: i32,
    /// The name of the peak or volcano
    pub name: Option<String>,
    /// The coordinates of the peak or volcano
    pub coord: Option<Coordinate>,
    /// The height of the peak
    pub height: Option<i32>,
    /// Denotes if the peak is a volcano or a mountain
    pub is_volcano: Option<bool>,
}

impl CreateNew for MountainPeak {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for MountainPeak {
    fn example() -> Self {
        Self::default()
    }
}

//! Objects for basic information for the API about DF Storyteller.

use crate::SchemaExample;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::collections::HashMap;

/// Information about DF Storyteller itself
#[derive(Serialize, Deserialize, Clone, Debug, Default, JsonSchema)] // GraphQLObject
#[schemars(example = "Self::example")]
pub struct DFSTInfo {
    /// The version of DF Storyteller currently running.
    /// This used the [Semantic Versioning](https://semver.org/) system.
    /// Example: "0.1.0" or "1.5.10"
    /// For pre-releases an addition might be added after the last number.
    /// The last number will then always be followed by a dash `-`.
    /// Example: "0.1.5-alpha-1" or "2.10.1-beta-2"
    ///
    /// Only the major and minor version should be used in most cases.
    /// So for "0.5.7" only "0.5" should be checked. The "7" will just define the patches.
    /// The patches should not break anything (only fix them)
    ///
    /// The following Regex expression can be used the check this field:
    /// Regex match: `^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+))?$`
    /// This will match "2.13.1-beta-5" to:
    /// - Group #1: "2"
    /// - Group #2: "13"
    /// - Group #3: "1"
    /// - Group #4: "beta-5"
    pub version: String,
    /// Features that are enabled in DF Storyteller.
    /// This will be used to specified if a feature is enabled on this install.
    /// Future features might include: "maps", "dm_mode", "discovery_mode", ...
    pub enabled_features: Vec<String>,
    /// A list of enabled modification to the game and DF Storyteller.
    /// This list has to be specified in the settings of DF Storyteller.
    /// DF Storyteller can not detect mods from Dwarf Fortress.
    pub enabled_mods: Vec<ModInfo>,
    /// The Base URL is the first part of the url where api request should go to.
    /// This is usually set to `http://localhost:20350/api`. But can be changes by the user
    /// in the settings. This value is also printed in the output for the user.
    /// Paintings can refer to this to get the initial base_url is not set.
    pub base_url: String,
    /// The id of the world that is currently loaded.
    /// This is the same value as the `-w X` flag, where X is the world id.
    /// This value can only be positive as negative numbers are not allowed by the terminal.
    pub world_id: i32,
    // TODO add settings
}

/// Information about the a mod in Dwarf Fortress or DF Storyteller
#[derive(Serialize, Deserialize, Clone, Debug, Default, JsonSchema)] // GraphQLObject
#[schemars(example = "Self::example")]
pub struct ModInfo {
    /// The name of the mod
    /// The name must be unique, no mods can have the same name
    /// A list of mods can be found [here](https://gitlab.com/df_storyteller/df-storyteller#mods-tested)
    pub name: String,
    /// The version of the mod that is used.
    /// This can be used to enable/disable features in the UI.
    pub version: String,
    /// A valid URL where the mod can be downloaded or more info can be found.
    pub url: String,
    /// Additional properties that can be set by the mod.
    pub properties: HashMap<String, Value>,
}

impl DFSTInfo {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for DFSTInfo {
    fn example() -> Self {
        Self {
            version: "0.1.0".to_owned(),
            enabled_features: vec!["maps".to_owned(), "dm_mode".to_owned()],
            enabled_mods: vec![ModInfo::example()],
            base_url: "http://localhost:20350/api".to_owned(),
            world_id: 2,
        }
    }
}

impl ModInfo {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for ModInfo {
    fn example() -> Self {
        let mut properties = HashMap::new();
        properties.insert("races".to_owned(), json!(["human", "dwarf"]));
        Self {
            name: "☼Masterwork☼ Dwarf Fortress".to_owned(),
            version: "1.31".to_owned(),
            url: "http://www.bay12forums.com/smf/index.php?board=24.0".to_owned(),
            properties,
        }
    }
}

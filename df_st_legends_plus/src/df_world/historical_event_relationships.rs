use df_st_core::{Filler, HasUnknown};
use df_st_derive::HasUnknown;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HERelationship {
    pub event: i32,
    pub year: Option<i32>,
    // not currently in XML, but for future proof
    pub seconds72: Option<i32>,
    pub relationship: Option<String>,
    pub source_hf: Option<i32>,
    pub target_hf: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HERelationships {
    pub historical_event_relationship: Option<Vec<HERelationship>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalEvent, HERelationship> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HERelationship) {
        self.id.add_missing_data(&source.event);
        self.type_ = Some("hf_relationship".to_owned());
        self.year.add_missing_data(&source.year);
        self.seconds72.add_missing_data(&source.seconds72);
        // Pre DFHack v0.47.04-r2 `relationship` was a number.
        if let Some(relationship) = &source.relationship {
            match relationship.parse::<i32>() {
                Ok(_number) => {
                    // TODO convert number to its string counterparts
                }
                Err(_) => {
                    self.relationship.add_missing_data(&source.relationship);
                }
            }
        }
        self.source_hf_id.add_missing_data(&source.source_hf);
        self.target_hf_id.add_missing_data(&source.target_hf);
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HERelationship {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.event == other.id
    }
}

impl std::hash::Hash for HERelationship {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.event.hash(state);
    }
}

impl Filler<Vec<df_st_core::HistoricalEvent>, HERelationships>
    for Vec<df_st_core::HistoricalEvent>
{
    fn add_missing_data(&mut self, source: &HERelationships) {
        self.add_missing_data(&source.historical_event_relationship);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalEvent>, HERelationships>
    for IndexMap<u64, df_st_core::HistoricalEvent>
{
    fn add_missing_data(&mut self, source: &HERelationships) {
        self.add_missing_data(&source.historical_event_relationship);
    }
}

use df_st_core::{Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct HistoricalEra {
    pub id: i32,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HistoricalEras {
    pub historical_era: Option<Vec<HistoricalEra>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalEra, HistoricalEra> for df_st_core::HistoricalEra {
    fn add_missing_data(&mut self, source: &HistoricalEra) {
        self.id.add_missing_data(&source.id);
    }
}

impl PartialEq<df_st_core::HistoricalEra> for HistoricalEra {
    fn eq(&self, other: &df_st_core::HistoricalEra) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::HistoricalEra>, HistoricalEras> for Vec<df_st_core::HistoricalEra> {
    fn add_missing_data(&mut self, source: &HistoricalEras) {
        self.add_missing_data(&source.historical_era);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalEra>, HistoricalEras>
    for IndexMap<u64, df_st_core::HistoricalEra>
{
    fn add_missing_data(&mut self, source: &HistoricalEras) {
        self.add_missing_data(&source.historical_era);
    }
}

use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_regions() -> Vec<Region> {
    vec![
        Region {
            // Only use values from legends.xml
            id: 0,
            name: Some("the water of wines".to_owned()),
            type_: Some("Ocean".to_owned()),
            coords: vec![],
            evilness: None,
            force_ids: vec![],
        },
        Region {
            // Only use values from legends_plus.xml
            id: 1,
            name: None,
            type_: None,
            coords: vec![
                Coordinate {
                    x: 23,
                    y: 9,
                    ..Default::default()
                },
                Coordinate {
                    x: 24,
                    y: 8,
                    ..Default::default()
                },
                Coordinate {
                    x: 24,
                    y: 9,
                    ..Default::default()
                },
                Coordinate {
                    x: 25,
                    y: 7,
                    ..Default::default()
                },
            ],
            evilness: Some("neutral".to_owned()),
            force_ids: vec![4, 10],
        },
        Region {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        Region {
            // Full merge
            id: 3,
            name: Some("the crewed blizzard".to_owned()),
            type_: Some("Glacier".to_owned()),
            coords: vec![Coordinate {
                x: 2,
                y: 9,
                ..Default::default()
            }],
            evilness: Some("neutral".to_owned()),
            force_ids: vec![1],
        },
        Region {
            // missing ID
            id: 4,
            ..Default::default()
        },
        Region {
            // Set ID's to `-1`
            id: 5,
            coords: vec![Coordinate {
                x: -1,
                y: -1,
                ..Default::default()
            }],
            force_ids: vec![],
            ..Default::default()
        },
        Region {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            type_: Some("".to_owned()),
            evilness: Some("".to_owned()),
            ..Default::default()
        },
        Region::new_by_id(7),
        Region {
            // higher UTF-8 and CP437 chars
            id: 8,
            name: Some("These are cp437 chars: ê å ╢ Θ".to_owned()),
            type_: Some("These are cp437 chars: êå╢Θ".to_owned()),
            evilness: Some("evil 😈".to_owned()),
            ..Default::default()
        },
        Region {
            id: 9,
            name: Some("the lessened hot hills of meeting".to_owned()),
            type_: Some("Hills".to_owned()),
            evilness: Some("evil".to_owned()),
            ..Default::default()
        },
        Region {
            id: 10,
            name: Some("the lessened hot hills of meeting".to_owned()),
            type_: Some("Hills".to_owned()),
            evilness: Some("neutral".to_owned()),
            ..Default::default()
        },
    ]
}

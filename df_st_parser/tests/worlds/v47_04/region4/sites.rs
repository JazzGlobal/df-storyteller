use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_sites() -> Vec<Site> {
    vec![
        Site {
            // Only use values from legends.xml
            id: 0,
            name: Some("the naughty echo".to_owned()),
            type_: Some("cave".to_owned()),
            coord: Some(Coordinate {
                x: 10,
                y: 24,
                ..Default::default()
            }),
            rectangle: Some(Rectangle {
                id: 0,
                x1: 173,
                y1: 397,
                x2: 175,
                y2: 399,
            }),
            structures: vec![Structure {
                site_id: 0,
                local_id: 0,
                name: Some("the spice of shielding".to_owned()),
                type_: Some("inn tavern".to_owned()),
                subtype: Some("catacombs".to_owned()),
                entity_id: Some(1),
                worship_hf_id: Some(2),
                copied_artifact_ids: vec![1, 7, 9],
                ..Default::default()
            }],
            site_properties: vec![SiteProperty {
                site_id: 0,
                local_id: 0,
                type_: Some("house".to_owned()),
                structure_id: Some(0),
                owner_hf_id: Some(5),
            }],
            civ_id: None,
            cur_owner_id: None,
        },
        Site {
            // Only use values from legends_plus.xml
            id: 1,
            name: None,
            type_: None,
            coord: None,
            rectangle: None,
            structures: vec![Structure {
                site_id: 1,
                local_id: 0,
                name: Some("Kradayrbus1".to_owned()),
                name2: Some("Kradayrbus2".to_owned()),
                type_: Some("market".to_owned()),
                subtype: None,
                entity_id: None,
                worship_hf_id: None,
                copied_artifact_ids: vec![],
                deity_type: Some(3),
                deity_id: Some(4),
                religion_id: Some(5),
                dungeon_type: Some(6),
                inhabitant_hf_ids: vec![4, 6],
            }],
            site_properties: vec![],
            civ_id: Some(5),
            cur_owner_id: Some(6),
        },
        Site {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        Site {
            // Full merge
            id: 3,
            name: Some("the sable hurricane".to_owned()),
            type_: Some("mead hall".to_owned()),
            coord: Some(Coordinate {
                x: 97,
                y: 47,
                ..Default::default()
            }),
            rectangle: Some(Rectangle {
                id: 1,
                x1: 13,
                y1: 37,
                x2: 15,
                y2: 39,
            }),
            structures: vec![
                Structure {
                    site_id: 3,
                    local_id: 0,
                    name: Some("The Barricaded Meal".to_owned()),
                    name2: Some("Tholtigfathkal".to_owned()),
                    type_: Some("inn tavern".to_owned()),
                    subtype: Some("cat".to_owned()),
                    entity_id: Some(7),
                    worship_hf_id: Some(4),
                    copied_artifact_ids: vec![2, 4],
                    deity_type: Some(1),
                    deity_id: Some(9),
                    religion_id: Some(7),
                    dungeon_type: Some(1),
                    inhabitant_hf_ids: vec![1, 9],
                },
                Structure {
                    site_id: 3,
                    local_id: 1,
                    copied_artifact_ids: vec![1, 4],
                    ..Default::default()
                },
                Structure {
                    site_id: 3,
                    local_id: 2,
                    inhabitant_hf_ids: vec![3, 7],
                    ..Default::default()
                },
            ],
            site_properties: vec![SiteProperty {
                site_id: 3,
                local_id: 0,
                type_: Some("castle".to_owned()),
                structure_id: Some(2),
                owner_hf_id: Some(1),
            }],
            civ_id: Some(7),
            cur_owner_id: Some(1),
        },
        Site {
            // missing ID
            id: 4,
            ..Default::default()
        },
        Site {
            // Set ID's to `-1`
            id: 5,
            coord: Some(Coordinate {
                x: -1,
                y: -1,
                ..Default::default()
            }),
            rectangle: Some(Rectangle {
                id: 2,
                x1: -1,
                y1: -1,
                x2: -1,
                y2: -1,
            }),
            structures: vec![Structure {
                site_id: 5,
                local_id: 0,
                entity_id: None,
                worship_hf_id: None,
                copied_artifact_ids: vec![],
                deity_type: Some(-1),
                deity_id: None,
                religion_id: None,
                dungeon_type: Some(-1),
                inhabitant_hf_ids: vec![],
                ..Default::default()
            }],
            site_properties: vec![SiteProperty {
                site_id: 5,
                local_id: 0,
                structure_id: None,
                owner_hf_id: None,
                ..Default::default()
            }],
            civ_id: None,
            cur_owner_id: None,
            ..Default::default()
        },
        Site {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            type_: Some("".to_owned()),
            coord: None,
            rectangle: None,
            structures: vec![Structure {
                site_id: 6,
                local_id: 0,
                name: Some("".to_owned()),
                name2: Some("".to_owned()),
                type_: Some("".to_owned()),
                subtype: Some("".to_owned()),
                ..Default::default()
            }],
            site_properties: vec![SiteProperty {
                site_id: 6,
                local_id: 0,
                type_: Some("".to_owned()),
                ..Default::default()
            }],
            ..Default::default()
        },
        Site::new_by_id(7),
        Site {
            // higher UTF-8 and CP437 chars
            id: 8,
            name: Some("These are cp437 chars: ê å ╢ Θ".to_owned()),
            type_: Some("These are cp437 chars: ê å ╢ Θ".to_owned()),
            ..Default::default()
        },
        Site {
            id: 9,
            name: Some("chanceddrunken".to_owned()),
            ..Default::default()
        },
        Site {
            id: 10,
            name: Some("hatedweed".to_owned()),
            structures: vec![Structure {
                site_id: 10,
                local_id: 0,
                name: Some("The Bean of Hay".to_owned()),
                name2: Some("Seto Thel".to_owned()),
                type_: Some("mead hall".to_owned()),
                ..Default::default()
            }],
            ..Default::default()
        },
    ]
}

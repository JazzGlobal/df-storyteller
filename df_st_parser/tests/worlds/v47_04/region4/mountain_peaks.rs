use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_mountain_peaks() -> Vec<MountainPeak> {
    vec![
        MountainPeak {
            // Only use values from legends.xml
            id: 0,
            ..Default::default()
        },
        MountainPeak {
            // Only use values from legends_plus.xml
            id: 1,
            name: Some("The Finger of Growing".to_owned()),
            coord: Some(Coordinate {
                x: 75,
                y: 64,
                ..Default::default()
            }),
            height: Some(265),
            is_volcano: Some(false),
        },
        MountainPeak {
            // ID merge, only ID
            id: 2,
            is_volcano: Some(false),
            ..Default::default()
        },
        MountainPeak {
            // Full merge
            id: 3,
            name: Some("The Ultimate Helms".to_owned()),
            coord: Some(Coordinate {
                x: 52,
                y: 49,
                ..Default::default()
            }),
            height: Some(265),
            is_volcano: Some(false),
        },
        MountainPeak {
            // missing ID
            id: 4,
            ..Default::default()
        },
        MountainPeak {
            // Set ID's to `-1`
            id: 5,
            name: Some("The Furnace of Treaties".to_owned()),
            coord: Some(Coordinate {
                x: -1,
                y: -1,
                ..Default::default()
            }),
            height: Some(-1),
            is_volcano: Some(true),
        },
        MountainPeak {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            is_volcano: Some(false),
            ..Default::default()
        },
        MountainPeak::new_by_id(7),
        MountainPeak {
            // higher UTF-8 and CP437 chars
            id: 8,
            name: Some("The Land 😈 of 𐅋 Polish".to_owned()),
            is_volcano: Some(false),
            ..Default::default()
        },
        MountainPeak {
            id: 9,
            name: Some("The Island of Shanks".to_owned()),
            is_volcano: Some(false),
            ..Default::default()
        },
        MountainPeak {
            id: 10,
            name: Some("The Land of Rhyming".to_owned()),
            is_volcano: Some(false),
            ..Default::default()
        },
    ]
}

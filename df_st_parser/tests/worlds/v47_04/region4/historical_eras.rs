use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_historical_eras() -> Vec<HistoricalEra> {
    vec![
        HistoricalEra {
            // Only use values from legends.xml
            id: 0,
            name: Some("Age of Hydra and Tundra Titan".to_owned()),
            start_year: Some(-1),
        },
        HistoricalEra {
            // Only use values from legends_plus.xml
            id: 1,
            name: None,
            start_year: None,
        },
        HistoricalEra {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        HistoricalEra {
            // Full merge
            id: 3,
            name: Some("Age of Legends".to_owned()),
            start_year: Some(144),
        },
        HistoricalEra {
            // missing ID
            id: 4,
            ..Default::default()
        },
        HistoricalEra {
            // Set ID's to `-1`
            id: 5,
            name: Some("Age of Myth".to_owned()),
            // This should be `-1` not None as this is the fallback for
            // everything that happened before the year 0
            start_year: Some(-1),
        },
        HistoricalEra {
            // empty strings
            id: 6,
            name: Some("The Age of Heroes".to_owned()),
            start_year: Some(1),
        },
        HistoricalEra::new_by_id(7),
        HistoricalEra {
            id: 8,
            name: Some("The Age of Three Powers".to_owned()),
            start_year: Some(1005),
        },
        HistoricalEra {
            id: 9,
            name: Some("The Age of Two (Race)s".to_owned()),
            start_year: Some(788657),
        },
        HistoricalEra {
            id: 10,
            name: Some("The Twilight Age".to_owned()),
            start_year: Some(5456),
        },
    ]
}

use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_entities() -> Vec<Entity> {
    vec![
        Entity {
            // Only use values from legends.xml
            id: 0,
            name: Some("the fiery council".to_owned()),
            honor: vec![EntityHonor {
                local_id: 0,
                name: Some("Captain".to_owned()),
                gives_precedence: Some(100),
                requires_any_melee_or_ranged_skill: Some(true),
                required_skill_ip_total: Some(12600),
                required_battles: Some(4),
                required_years: Some(63),
                required_skill: Some("HAMMER".to_owned()),
                required_kills: Some(15),
                exempt_ep_id: Some(6),
                exempt_former_ep_id: Some(3),
                granted_to_everybody: Some(true),
            }],
            race: None,
            type_: None,
            worship_id: vec![],
            weapon: vec![],
            profession: vec![],
            entity_link: vec![],
            entity_position: vec![],
            entity_position_assignment: vec![],
            hf_ids: vec![],
            child_en_ids: vec![],
            claims: vec![],
            occasion: vec![],
        },
        Entity {
            // Only use values from legends_plus.xml
            id: 1,
            name: None,
            honor: vec![],
            race: Some("dwarf".to_owned()),
            type_: Some("civilization".to_owned()),
            worship_id: vec![7, 8],
            weapon: vec![
                "short sword".to_owned(),
                "spear".to_owned(),
                "war hammer".to_owned(),
            ],
            profession: vec!["bowyer".to_owned(), "weaponsmith".to_owned()],
            entity_link: vec![
                EntityLink {
                    local_id: 0,
                    type_: Some("CHILD".to_owned()),
                    target: Some(5),
                    strength: Some(100),
                },
                EntityLink {
                    local_id: 1,
                    type_: Some("CHILD".to_owned()),
                    target: Some(9),
                    strength: Some(100),
                },
            ],
            entity_position: vec![
                EntityPosition {
                    local_id: 0,
                    name: Some("law-giver".to_owned()),
                    name_male: Some("law-giver1".to_owned()),
                    name_female: Some("law-giver2".to_owned()),
                    spouse: Some("law-giver3".to_owned()),
                    spouse_male: Some("law-giver4".to_owned()),
                    spouse_female: Some("law-giver5".to_owned()),
                },
                EntityPosition {
                    local_id: 1,
                    name: Some("bookkeeper".to_owned()),
                    name_male: Some("bookkeeper1".to_owned()),
                    name_female: Some("bookkeeper-giver2".to_owned()),
                    spouse: Some("bookkeeper3".to_owned()),
                    spouse_male: Some("bookkeeper-giver4".to_owned()),
                    spouse_female: Some("bookkeeper5".to_owned()),
                },
            ],
            entity_position_assignment: vec![
                EntityPositionAssignment {
                    local_id: 0,
                    hf_id: Some(6),
                    position_id: Some(1),
                    squad_id: None,
                },
                EntityPositionAssignment {
                    local_id: 1,
                    hf_id: Some(3),
                    position_id: Some(0),
                    squad_id: Some(1),
                },
            ],
            hf_ids: vec![1, 9],
            child_en_ids: vec![4, 10],
            claims: vec![
                Coordinate {
                    x: 48,
                    y: 64,
                    ..Default::default()
                },
                Coordinate {
                    x: 49,
                    y: 64,
                    ..Default::default()
                },
                Coordinate {
                    x: 50,
                    y: 64,
                    ..Default::default()
                },
                Coordinate {
                    x: 51,
                    y: 64,
                    ..Default::default()
                },
            ],
            occasion: vec![
                EntityOccasion {
                    local_id: 0,
                    name: Some("The Radiant Festival".to_owned()),
                    event: Some(6),
                    schedule: vec![
                        EntityOccasionSchedule {
                            local_id: 0,
                            type_: Some("musical_performance".to_owned()),
                            item_type: Some("hammer".to_owned()),
                            item_subtype: Some("head".to_owned()),
                            reference: Some(1),
                            reference2: None,
                            ..Default::default()
                        },
                        EntityOccasionSchedule {
                            local_id: 1,
                            type_: Some("ceremony".to_owned()),
                            reference: None,
                            reference2: None,
                            feature: vec![
                                EntityOccasionScheduleFeature {
                                    local_id: 0,
                                    type_: Some("incense_burning".to_owned()),
                                    reference: Some(3),
                                },
                                EntityOccasionScheduleFeature {
                                    local_id: 1,
                                    type_: Some("candles".to_owned()),
                                    reference: None,
                                },
                                EntityOccasionScheduleFeature {
                                    local_id: 2,
                                    type_: Some("costumes".to_owned()),
                                    reference: None,
                                },
                            ],
                            ..Default::default()
                        },
                    ],
                },
                EntityOccasion {
                    local_id: 1,
                    name: Some("The Festival of Mines".to_owned()),
                    event: Some(9),
                    schedule: vec![
                        EntityOccasionSchedule {
                            local_id: 0,
                            type_: Some("dance_performance".to_owned()),
                            reference: Some(1),
                            reference2: None,
                            ..Default::default()
                        },
                        EntityOccasionSchedule {
                            local_id: 2,
                            type_: Some("foot_race".to_owned()),
                            reference: None,
                            reference2: None,
                            ..Default::default()
                        },
                    ],
                },
            ],
        },
        Entity {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        Entity {
            // Full merge
            id: 3,
            name: Some("tempor".to_owned()),
            honor: vec![
                EntityHonor {
                    local_id: 0,
                    name: Some("Curabitur".to_owned()),
                    gives_precedence: Some(10),
                    requires_any_melee_or_ranged_skill: None,
                    required_skill_ip_total: Some(1600),
                    required_battles: Some(7),
                    required_years: Some(9),
                    required_skill: Some("KNIFE".to_owned()),
                    required_kills: Some(5),
                    exempt_ep_id: Some(9),
                    exempt_former_ep_id: Some(1),
                    granted_to_everybody: None,
                },
                EntityHonor {
                    local_id: 1,
                    name: Some("Vestibulum".to_owned()),
                    requires_any_melee_or_ranged_skill: None,
                    granted_to_everybody: None,
                    ..Default::default()
                },
            ],
            race: Some("elf".to_owned()),
            type_: Some("camp".to_owned()),
            worship_id: vec![1, 2],
            weapon: vec!["b".to_owned(), "k".to_owned(), "z".to_owned()],
            profession: vec!["r".to_owned(), "y".to_owned()],
            entity_link: vec![EntityLink {
                local_id: 0,
                type_: Some("consequat".to_owned()),
                target: Some(1),
                strength: Some(5),
            }],
            entity_position: vec![EntityPosition {
                local_id: 0,
                name: Some("pretium".to_owned()),
                ..Default::default()
            }],
            entity_position_assignment: vec![EntityPositionAssignment {
                local_id: 0,
                hf_id: Some(3),
                position_id: Some(0),
                squad_id: Some(5),
            }],
            hf_ids: vec![2, 8],
            child_en_ids: vec![2, 3],
            claims: vec![Coordinate {
                x: 5,
                y: 3,
                ..Default::default()
            }],
            occasion: vec![EntityOccasion {
                local_id: 0,
                name: Some("The vestibulum commodo".to_owned()),
                event: Some(3),
                schedule: vec![EntityOccasionSchedule {
                    local_id: 0,
                    type_: Some("malesuada".to_owned()),
                    feature: vec![EntityOccasionScheduleFeature {
                        local_id: 0,
                        type_: Some("pretium".to_owned()),
                        ..Default::default()
                    }],
                    ..Default::default()
                }],
            }],
        },
        Entity {
            // missing ID
            id: 4,
            ..Default::default()
        },
        Entity {
            id: 5,
            name: Some("magna ac massa".to_owned()),
            race: Some("placerat luctus".to_owned()),
            ..Default::default()
        },
        Entity {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            race: Some("".to_owned()),
            type_: Some("".to_owned()),
            honor: vec![EntityHonor {
                local_id: 0,
                name: Some("".to_owned()),
                required_skill: Some("".to_owned()),
                ..Default::default()
            }],
            ..Default::default()
        },
        Entity::new_by_id(7),
        Entity {
            id: 8,
            name: Some("The Copper Festival".to_owned()),
            ..Default::default()
        },
        Entity {
            id: 9,
            name: Some("The Crystalline Celebration".to_owned()),
            ..Default::default()
        },
        Entity {
            id: 10,
            name: Some("The Dominant Celebration".to_owned()),
            ..Default::default()
        },
    ]
}

#!/bin/sh

# Build
# cargo afl build

$FOLDERIN="in1"

mkdir $FOLDERIN

AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1 AFL_SKIP_CPUFREQ=1 cargo afl fuzz -i $FOLDERIN -o out ./target/debug/afl_fuzz_api

#[macro_use]
extern crate afl;

use hyper::Url;

// Fuzz the URLs that can be input by the user
fn main() {
    fuzz!(|data: &[u8]| {
        // fuzzed code goes here
        if let Ok(s) = std::str::from_utf8(data) {
            let _ = Url::parse(s);
        }
    });
}

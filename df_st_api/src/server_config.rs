use df_st_core::config::{get_database_url, RootConfig};
use rocket::{
    config::{Config, LogLevel, SecretKey, TlsConfig},
    data::{ByteUnit, Limits},
    figment::Figment,
    serde::json::Value,
};
use serde_json::Map;

/// Read config file and create Rocket Config Object
pub fn set_server_config(server_config: &RootConfig) -> Figment {
    // This replaces the Rocket.toml file.
    // See: https://api.rocket.rs/v0.4/rocket/config/struct.ConfigBuilder.html
    let mut database_config = Map::new();
    let mut databases = Map::new();

    // Set Database url
    if let Some(url) = get_database_url(server_config, &df_st_db::DB_SERVICE) {
        database_config.insert("url".to_owned(), Value::String(url));
    }
    // Set Pool Size
    if let Some(pool_size) = &server_config.database.pool_size {
        database_config.insert("pool_size".to_owned(), Value::from(*pool_size));
    }
    databases.insert("df_st_database".to_owned(), Value::from(database_config));
    // Clone it so we don't have to deal with all the references below
    let server_conf = server_config.server.clone();

    // Start creating config
    let mut config = Config::default();
    // "0.0.0.0" Allows all ips (so anything that can reach this computer)
    // If behind NAT this means that port forwarding COULD be done
    // Although exposing this is a very bad idea!
    // otherwise use "localhost"
    config.address = server_conf
        .address
        .unwrap_or_else(|| "127.0.0.1".to_string())
        .parse()
        .unwrap();

    config.port = server_conf.port.unwrap_or(20350);
    // Set Workers
    if server_conf.workers.is_some() {
        config.workers = server_conf.workers.unwrap_or(16) as usize;
    }
    // Set Keep Alive duration
    config.keep_alive = server_conf.keep_alive.unwrap_or(5);
    // Set logging Level for Rocket (not DF Storyteller)
    config.log_level = match server_conf.log_level.unwrap_or_default().as_ref() {
        "Critical" => LogLevel::Critical,
        "Normal" => LogLevel::Normal,
        "Debug" => LogLevel::Debug,
        "Off" => LogLevel::Off,
        _ => LogLevel::Normal,
    };
    // Set secret key for cookies
    if let Some(secret_key) = server_conf.secret_key {
        config.secret_key = SecretKey::from(secret_key.as_bytes());
    }
    // Set TLS settings
    if let Some(tls_conf) = server_conf.tls {
        config.tls = Some(TlsConfig::from_paths(tls_conf.certs, tls_conf.private_key));
    }
    // Set Limits: Max size accepted for data type.
    if let Some(limits) = server_conf.limits {
        let mut limit = Limits::default();
        for (key, value) in limits.values {
            limit = limit.limit(key, ByteUnit::Byte(value));
        }
        config.limits = limit;
    }
    // Set database settings from above.
    Figment::from(config).merge(("databases", databases))
}

#[derive(Clone, Debug, Default)]
pub struct ServerInfo {
    pub page_max_limit: u32,
    pub default_max_page_size: u32,
    pub base_url: String,
    pub world_id: i32,
}

impl ServerInfo {
    pub fn new(server_config: &RootConfig, api_path: &str, world_id: u32) -> Self {
        let mut server_info = ServerInfo::default();
        let server_conf = server_config.server.clone();

        let protocol = match server_conf.tls {
            // lets assume that when a tls configuration is set it will work.
            Some(_) => "https".to_owned(),
            None => "http".to_owned(),
        };

        server_info.base_url = format!(
            "{}://{}:{}{}",
            protocol,
            server_config.local_address.trim_matches('/'),
            server_conf.port.unwrap_or(20350),
            api_path.trim_end_matches('/'),
        );
        server_info.page_max_limit = 500;
        server_info.default_max_page_size = 100;
        server_info.world_id = world_id as i32;
        server_info
    }
}

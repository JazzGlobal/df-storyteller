use crate::api_errors::{APIErrorNoContent, APIErrorNotModified};
use crate::api_objects::ApiObject;
use crate::pagination::{
    ApiCountPage, ApiCountPagination, ApiItem, ApiItemQuery, ApiPage, ApiPagination,
};
use crate::DfStDatabase;
use crate::ServerInfo;
use df_st_core::item_count::ItemCount;
use df_st_db::{id_filter, DBObject};
use okapi::openapi3::OpenApi;
use rocket::serde::json::Json;
use rocket::{get, State};
use rocket_okapi::{openapi, openapi_get_routes_spec, settings::OpenApiSettings};

impl ApiObject for df_st_core::Site {
    fn get_type() -> String {
        "site".to_owned()
    }
    fn get_item_link(&self, base_url: &str) -> String {
        format!("{}/sites/{}", base_url, self.id)
    }
    fn get_page_link(base_url: &str) -> String {
        format!("{}/sites", base_url)
    }
    fn get_count_link(base_url: &str) -> String {
        format!("{}/sites/count", base_url)
    }
}

impl ApiObject for df_st_core::Structure {
    fn get_type() -> String {
        "structure".to_owned()
    }
    fn get_item_link(&self, base_url: &str) -> String {
        format!(
            "{}/sites/{}/structures/{}",
            base_url, self.site_id, self.local_id
        )
    }
    fn get_page_link(base_url: &str) -> String {
        format!("{}/structures", base_url)
    }
    fn get_count_link(base_url: &str) -> String {
        format!("{}/structures/count", base_url)
    }
}

impl ApiObject for df_st_core::SiteProperty {
    fn get_type() -> String {
        "site_propertie".to_owned()
    }
    fn get_item_link(&self, base_url: &str) -> String {
        format!(
            "{}/sites/{}/properties/{}",
            base_url, self.site_id, self.local_id
        )
    }
    fn get_page_link(base_url: &str) -> String {
        format!("{}/site_properties", base_url)
    }
    fn get_count_link(base_url: &str) -> String {
        format!("{}/site_properties/count", base_url)
    }
}

pub fn get_routes_and_docs(settings: &OpenApiSettings) -> (Vec<rocket::Route>, OpenApi) {
    openapi_get_routes_spec![
        settings:
        // Sites
        get_site,
        list_sites,
        get_site_count,
        // Sites/Structures
        get_site_structure,
        list_site_structures,
        get_site_structure_count,
        // Sites/SiteProperty
        get_site_property,
        list_site_properties,
        get_site_property_count,
        // Structures
        list_structures,
        get_structure_count,
        // SiteProperty
        list_all_site_properties,
        get_all_site_property_count,
    ]
}

/// Request a `Site` by id.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites/<site_id>?<item_query..>")]
pub async fn get_site(
    db: DfStDatabase,
    site_id: i32,
    item_query: ApiItemQuery,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::Site>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::Site>::new(server_info, &item_query);
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        let site_search = df_st_db::Site::get_from_db(
            conn,
            id_filter!["id" => site_id, "world_id" => world_id],
            api_page.get_nested_items(),
        )?;
        if let Some(site) = site_search {
            api_page.wrap(site);
            return Ok(Json(api_page));
        }
        Err(APIErrorNoContent::new())
    })
    .await
}

/// Request a list of all `Sites` in the world.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites?<pagination..>")]
pub async fn list_sites(
    db: DfStDatabase,
    pagination: ApiPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::Site>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::Site>::new(&pagination, server_info);
    api_page.match_fields::<df_st_db::Site, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::Site::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            1,
            None,
            None,
        )?
        .get(0)
        .unwrap()
        .count;

        let result_list = df_st_db::Site::get_list_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.get_db_order(),
            api_page.order_by.clone(),
            None,
            api_page.get_nested_items(),
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a counts about `Site` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites/count?<group_by>&<pagination..>")]
pub async fn get_site_count(
    db: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiCountPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::Site>>, APIErrorNotModified> {
    let mut api_page = ApiCountPage::<ItemCount, df_st_core::Site>::new(&pagination, server_info);
    api_page.group_by = group_by;
    api_page.match_fields::<df_st_db::Site, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::Site::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            u32::MAX,
            api_page.group_by.clone(),
            None,
        )?
        .len() as u32;

        let result_list = df_st_db::Site::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.group_by.clone(),
            None,
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a `Structure` by id within a `Site`.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites/<site_id>/structures/<structure_id>")]
pub async fn get_site_structure(
    db: DfStDatabase,
    site_id: i32,
    structure_id: i32,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::Structure>>, APIErrorNoContent> {
    // Can not detect difference between 2 routes, (this and count)
    let item_query = ApiItemQuery::default();
    let mut api_page = ApiItem::<df_st_core::Structure>::new(server_info, &item_query);
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        let item_search = df_st_db::Structure::get_from_db(
            conn,
            id_filter!["site_id" => site_id, "local_id" => structure_id, "world_id" => world_id],
            api_page.get_nested_items(),
        )?;
        if let Some(item) = item_search {
            api_page.wrap(item);
            return Ok(Json(api_page));
        }
        Err(APIErrorNoContent::new())
    })
    .await
}

/// Request a list of all `Structure` in the `Site`.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites/<site_id>/structures?<pagination..>")]
pub async fn list_site_structures(
    db: DfStDatabase,
    site_id: i32,
    pagination: ApiPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::Structure>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::Structure>::new(&pagination, server_info);
    api_page.match_fields::<df_st_db::Structure, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::Structure::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["site_id" => site_id, "world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            1,
            None,
            None,
        )?
        .get(0)
        .unwrap()
        .count;

        let result_list = df_st_db::Structure::get_list_from_db(
            conn,
            api_page.add_int_filter(id_filter!["site_id" => site_id, "world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.get_db_order(),
            api_page.order_by.clone(),
            None,
            api_page.get_nested_items(),
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a counts about `Structures` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites/<site_id>/structures/count?<group_by>&<pagination..>")]
pub async fn get_site_structure_count(
    db: DfStDatabase,
    site_id: i32,
    group_by: Option<String>,
    pagination: ApiCountPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::Structure>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::Structure>::new(&pagination, server_info);
    api_page.group_by = group_by;
    api_page.match_fields::<df_st_db::Structure, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::Structure::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["site_id" => site_id, "world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            u32::MAX,
            api_page.group_by.clone(),
            None,
        )?
        .len() as u32;

        let result_list = df_st_db::Structure::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["site_id" => site_id, "world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.group_by.clone(),
            None,
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a `SiteProperty` by id within a `Site`.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites/<site_id>/properties/<site_property_id>")]
pub async fn get_site_property(
    db: DfStDatabase,
    site_id: i32,
    site_property_id: i32,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::SiteProperty>>, APIErrorNoContent> {
    // Can not detect difference between 2 routes, (this and count)
    let item_query = ApiItemQuery::default();
    let mut api_page = ApiItem::<df_st_core::SiteProperty>::new(server_info, &item_query);
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
    let item_search = df_st_db::SiteProperty::get_from_db(
        conn,
        id_filter!["site_id" => site_id, "local_id" => site_property_id, "world_id" => world_id],
        api_page.get_nested_items(),
    )?;
    if let Some(item) = item_search {
        api_page.wrap(item);
        return Ok(Json(api_page));
    }
    Err(APIErrorNoContent::new())})
    .await
}

/// Request a list of all `SiteProperty` in the `Site`.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites/<site_id>/properties?<pagination..>")]
pub async fn list_site_properties(
    db: DfStDatabase,
    site_id: i32,
    pagination: ApiPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::SiteProperty>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::SiteProperty>::new(&pagination, server_info);
    api_page.match_fields::<df_st_db::SiteProperty, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::SiteProperty::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["site_id" => site_id, "world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            1,
            None,
            None,
        )?
        .get(0)
        .unwrap()
        .count;

        let result_list = df_st_db::SiteProperty::get_list_from_db(
            conn,
            api_page.add_int_filter(id_filter!["site_id" => site_id, "world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.get_db_order(),
            api_page.order_by.clone(),
            None,
            api_page.get_nested_items(),
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a counts about `SiteProperty` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site")]
#[get("/sites/<site_id>/properties/count?<group_by>&<pagination..>")]
pub async fn get_site_property_count(
    db: DfStDatabase,
    site_id: i32,
    group_by: Option<String>,
    pagination: ApiCountPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::SiteProperty>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::SiteProperty>::new(&pagination, server_info);
    api_page.group_by = group_by;
    api_page.match_fields::<df_st_db::SiteProperty, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::SiteProperty::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["site_id" => site_id, "world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            u32::MAX,
            api_page.group_by.clone(),
            None,
        )?
        .len() as u32;

        let result_list = df_st_db::SiteProperty::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["site_id" => site_id, "world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.group_by.clone(),
            None,
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a list of all `Structure` in the world.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Structure")]
#[get("/structures?<pagination..>")]
pub async fn list_structures(
    db: DfStDatabase,
    pagination: ApiPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::Structure>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::Structure>::new(&pagination, server_info);
    api_page.match_fields::<df_st_db::Structure, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::Structure::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            1,
            None,
            None,
        )?
        .get(0)
        .unwrap()
        .count;

        let result_list = df_st_db::Structure::get_list_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.get_db_order(),
            api_page.order_by.clone(),
            None,
            api_page.get_nested_items(),
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a counts about `Structure` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Structure")]
#[get("/structures/count?<group_by>&<pagination..>")]
pub async fn get_structure_count(
    db: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiCountPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::Structure>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::Structure>::new(&pagination, server_info);
    api_page.group_by = group_by;
    api_page.match_fields::<df_st_db::Structure, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::Structure::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            u32::MAX,
            api_page.group_by.clone(),
            None,
        )?
        .len() as u32;

        let result_list = df_st_db::Structure::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.group_by.clone(),
            None,
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a list of all `SiteProperty` in the world.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site Property")]
#[get("/site_properties?<pagination..>")]
pub async fn list_all_site_properties(
    db: DfStDatabase,
    pagination: ApiPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::SiteProperty>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::SiteProperty>::new(&pagination, server_info);
    api_page.match_fields::<df_st_db::SiteProperty, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::SiteProperty::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            1,
            None,
            None,
        )?
        .get(0)
        .unwrap()
        .count;

        let result_list = df_st_db::SiteProperty::get_list_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.get_db_order(),
            api_page.order_by.clone(),
            None,
            api_page.get_nested_items(),
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a counts about `SiteProperty` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site Property")]
#[get("/site_properties/count?<group_by>&<pagination..>")]
pub async fn get_all_site_property_count(
    db: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiCountPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::SiteProperty>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::SiteProperty>::new(&pagination, server_info);
    api_page.group_by = group_by;
    api_page.match_fields::<df_st_db::SiteProperty, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::SiteProperty::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            u32::MAX,
            api_page.group_by.clone(),
            None,
        )?
        .len() as u32;

        let result_list = df_st_db::SiteProperty::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.group_by.clone(),
            None,
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

use df_st_core::config::RootConfig;
use log::{Level, LevelFilter, Metadata, Record};
use rocket::local::blocking::Client;
use std::path::{Path, PathBuf};
use tempfile::Builder;

pub static LOGGER: Logger = Logger;
pub struct Logger;

mod world_1;

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Debug
            && metadata.target() != "serde_xml_rs::de"
            && !metadata.target().starts_with("hyper::")
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!(
                "{:<5}:{} - {}",
                match record.level() {
                    Level::Error => "ERROR",
                    Level::Warn => "WARN",
                    Level::Info => "INFO",
                    Level::Debug => "DEBUG",
                    Level::Trace => "TRACE",
                },
                record.target(),
                record.args()
            );
        }
    }

    fn flush(&self) {}
}

pub fn setup() {
    log::set_logger(&LOGGER).ok();
    log::set_max_level(LevelFilter::Debug);
}

#[allow(dead_code)]
pub fn set_path(folder: &str) {
    let root = std::path::Path::new(folder);
    if !root.exists() {
        std::fs::create_dir(root).unwrap();
    }
    std::env::set_current_dir(&root).unwrap();
}

#[allow(dead_code)]
pub fn get_file(filename: &Path) -> String {
    use std::io::prelude::*;
    let mut file = std::fs::File::open(filename).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    contents
}

#[allow(dead_code)]
pub fn get_temp_dir() -> tempfile::TempDir {
    Builder::new().prefix("df_st_tests-").tempdir().unwrap()
}

#[allow(dead_code)]
pub fn get_fixed_dir(folder: &str) -> PathBuf {
    let root = std::path::PathBuf::from(folder);
    if !root.exists() {
        std::fs::create_dir(&root).unwrap();
    }
    root
}

pub fn create_test_database(config: &RootConfig) {
    // Create tables in DB
    if let Some(service) = &config.database.service {
        if service == "sqlite" && !df_st_db::check_db_has_tables(config) {
            log::info!("Found a database without tables. Running migrations");
            // Run migrations and create all the tables
            df_st_db::run_migrations(config);
        }
    }
    world_1::create_world(config);
}

pub fn get_service_client() -> Client {
    let mut config = RootConfig::default();

    let temp_dir = get_temp_dir();
    let db_file = temp_dir.path().join("df_st_db.db");

    // let non_temp_dir = get_fixed_dir("/tmp/df_st_tests");
    // let db_file = non_temp_dir.join("df_st_db.db");

    config.database.config.db_path = Some(db_file);

    create_test_database(&config);

    // Start service, make api call
    let world = 1;
    let service = df_st_api::create_service(&config, world);
    Client::untracked(service).expect("valid rocket instance")
}

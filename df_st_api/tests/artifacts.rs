mod common;

use pretty_assertions::assert_eq;
use rocket::http::{ContentType, Status};
use serde_json::json;

fn setup() {
    common::setup();
}

// -----------------------------
// ----- Integration Tests -----
// -----------------------------

#[test]
fn list_artifacts() {
    setup();

    let client = common::get_service_client();
    let req = client.get("/api/artifacts?per_page=2");
    let response = req.dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.content_type(), Some(ContentType::JSON));
    let result = json!({
        "max_page_size": 2,
        "total_item_count": 4,
        "page_start": 0,
        "page_size": 2,
        "page_nr": 0,
        "etag": "bcd7aa0e72010497da62597c3e5910a9bc45465d15fdf1fe1b631068685da8eb",
        "links": {
            "self": "http://127.0.0.1:20350/api/artifacts?per_page=2",
            "first": null,
            "prev": null,
            "next": "http://127.0.0.1:20350/api/artifacts?per_page=2&page=1",
            "last": "http://127.0.0.1:20350/api/artifacts?per_page=2&page=1",
        },
        "data": [
            {
            "_type": "artifact",
            "_links": {
                "self": "http://127.0.0.1:20350/api/artifacts/0"
            },
            "id": 0,
            "name": "the urns of oblivion",
            "site_id": null,
            "structure_local_id": null,
            "holder_hf_id": null,
            "abs_tile_x": null,
            "abs_tile_y": null,
            "abs_tile_z": null,
            "region_id": null,
            "item_name": "uja zitha",
            "item_type": "slab",
            "item_subtype": null,
            "item_writing": null,
            "item_page_number": null,
            "item_page_written_content_id": null,
            "item_writing_written_content_id": null,
            "item_description": "the secrets of life and death",
            "item_mat": "gabbro"
            },
            {
            "_type": "artifact",
            "_links": {
                "self": "http://127.0.0.1:20350/api/artifacts/1"
            },
            "id": 1,
            "name": "the gutter, my life",
            "site_id": null,
            "structure_local_id": null,
            "holder_hf_id": null,
            "abs_tile_x": null,
            "abs_tile_y": null,
            "abs_tile_z": null,
            "region_id": null,
            "item_name": "the gutter, my life",
            "item_type": "book",
            "item_subtype": null,
            "item_writing": null,
            "item_page_number": 35,
            "item_page_written_content_id": null,
            "item_writing_written_content_id": null,
            "item_description": null,
            "item_mat": "marble"
            }
        ]
    });
    assert_eq!(
        response.into_string(),
        Some(serde_json::to_string(&result).unwrap())
    );
}

#[test]
fn get_artifact() {
    setup();

    let client = common::get_service_client();
    let req = client.get("/api/artifacts/2");
    let response = req.dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.content_type(), Some(ContentType::JSON));
    let result = json!({
      "_type": "artifact",
      "_links": {
        "self": "http://127.0.0.1:20350/api/artifacts/2"
      },
      "id": 2,
      "name": "meals ever onward",
      "site_id": null,
      "structure_local_id": null,
      "holder_hf_id": null,
      "abs_tile_x": null,
      "abs_tile_y": null,
      "abs_tile_z": null,
      "region_id": null,
      "item_name": "meals ever onward",
      "item_type": "tool",
      "item_subtype": "scroll",
      "item_writing": null,
      "item_page_number": null,
      "item_page_written_content_id": null,
      "item_writing_written_content_id": null,
      "item_description": null,
      "item_mat": "giant armadillo parchment"
    });
    assert_eq!(
        response.into_string(),
        Some(serde_json::to_string(&result).unwrap())
    );
}

#[test]
fn get_artifact_count() {
    setup();

    let client = common::get_service_client();
    let req = client.get("/api/artifacts/count");
    let response = req.dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.content_type(), Some(ContentType::JSON));
    let result = json!({
      "max_page_size": 100,
      "total_item_count": 1,
      "page_start": 0,
      "page_size": 1,
      "page_nr": 0,
      "group_by": null,
      "etag": "43ee26e17c3798cd276dc20102117a83b209b88d690bd4119d462e8d9cd607b8",
      "links": {
        "self": "http://127.0.0.1:20350/api/artifacts/count?per_page=100",
        "first": null,
        "prev": null,
        "next": null,
        "last": null
      },
      "data": [
        {
          "_type": "item_count",
          "count": 4,
          "value": "total"
        }
      ]
    });
    assert_eq!(
        response.into_string(),
        Some(serde_json::to_string(&result).unwrap())
    );
}

#[test]
fn get_artifact_count_group_by() {
    setup();

    let client = common::get_service_client();
    let req = client.get("/api/artifacts/count?group_by=item_type");
    let response = req.dispatch();

    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.content_type(), Some(ContentType::JSON));
    let result = json!({
      "max_page_size": 100,
      "total_item_count": 3,
      "page_start": 0,
      "page_size": 3,
      "page_nr": 0,
      "group_by": "item_type",
      "etag": "5cacaa2e9d43087cec5cc1818aa71df19a4d56adae99c63b9978c850c41e6bb2",
      "links": {
        "self": "http://127.0.0.1:20350/api/artifacts/count?per_page=100&group_by=item_type",
        "first": null,
        "prev": null,
        "next": null,
        "last": null
      },
      "data": [
          {
            "_type": "item_count",
            "count": 2,
            "value": "book"
          },
          {
            "_type": "item_count",
            "count": 1,
            "value": "tool"
          },
          {
            "_type": "item_count",
            "count": 1,
            "value": "slab"
          }
      ]
    });
    assert_eq!(
        response.into_string(),
        Some(serde_json::to_string(&result).unwrap())
    );
}

#![forbid(unsafe_code)]
#![deny(clippy::all)]

/// Start of the DF Storyteller CLI application.
fn main() -> std::io::Result<()> {
    df_st_cli::main()
}

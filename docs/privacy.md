# Privacy

No this is not a privacy policy, well technically it is... But the whole point of this documents
is to show you what we do and don't do.

TL;DR: We do everything we can to NOT track you or collect any data on you. 
And we will also prevents others from doing so.

**What we do:**
 - Application checks for updates, this is not logged or tracked (see below for more info).
 - Create an application using open source project from a lot of places.
We do our best to screen all the code we use and we have made and will not use code that does not follow our believed.
 - We have a [website](https://dfstoryteller.com/), this website is self-made and self-hosted (on Amazon AWS).
There are no tracker anywhere on our website. We disabled logging, 
we will only enable it in short moments for debugging if something is broken and delete the logs afterwards.
 - We have a [Discord](https://discord.gg/aAXt6uu) as this is hosted on there servers and thus follows
there [Privacy policy](https://discord.com/new/privacy). They do collect data for advertisements and other purposes.
If you want us to open a [Matrix](https://matrix.org/) or other service, 
let us know (could also use some help with that).
 - We use GitLab, they also have there [Privacy policy](https://about.gitlab.com/privacy/) Although
they say they do not sell/rent our data.
 - GraphiQL and Playground documentation uses online assets and thus requests pages from other sources.
This might get fixed in future updates.

**What we DON'T do:**
 - Track you inside tha application, only checking for updates, but even that is not logged.
It basically works like this: you download a small file from one of our servers, in the file is some
data about if there is an update or not. And a small signature that shows that is has not been changed
and it comes from the developers. You can find 
[more info here](https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/docs/security.md#update-checking).
 - Track you on our website. There are no trackers and all logs are disabled.
 - If the app encounters an error or something goes wrong it will give you a link to report this issue.
We never automatically forward bug reports.
 - ...

## Like privacy?
If you, like me, want privacy here are some tips:
 - Download and use [Firefox](https://www.mozilla.org/en-US/firefox/new/) as your default web browser.
 - Install [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) in your browser.
 - Use [DuckDuckGo](https://duckduckgo.com/) as your default search engine, it is really good!
 (it has a [dark theme](https://duckduckgo.com/settings#theme!)
 - Enable [DNS over HTTPS](https://support.mozilla.org/en-US/kb/firefox-dns-over-https).
 - Use [OpenStreetMaps](https://www.openstreetmap.org),
 [OsmAnd](https://osmand.net/) is OpenStreetMaps for your phones.
 - Use Linux instead of Windows (MaxOS is also okay). I use [Linux Mint](https://linuxmint.com/)
, but [Ubuntu](https://ubuntu.com/download/desktop) or 
[something](https://distrowatch.com/dwres.php?resource=major) 
[else](https://itsfoss.com/best-linux-distributions/) is also okay.
You can have both Linux and Windows on 1 pc, so don't worry. 
I like it MUCH more then Windows, so much faster!
 - ...

If you have tips yourself, let us know!
Join us on [Discord](https://discord.gg/aAXt6uu) (I'm Ralpha#8433) or some other place.

CREATE TABLE link_he_hf (
  he_id INTEGER NOT NULL,
  hf_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, hf_id, world_id),
  -- FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id),
  FOREIGN KEY (hf_id, world_id) REFERENCES historical_figures (id, world_id)
);

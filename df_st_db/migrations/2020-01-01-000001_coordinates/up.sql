
CREATE TABLE coordinates (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  x INTEGER NOT NULL,
  y INTEGER NOT NULL,
  region_id INTEGER,
  underground_region_id INTEGER,
  world_construction_id INTEGER,
  entity_id INTEGER,
  
  PRIMARY KEY (id, world_id)
)


CREATE TABLE regions (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  type VARCHAR,
  evilness VARCHAR,

  PRIMARY KEY (id, world_id)
);

-- ALTER TABLE coordinates
-- ADD CONSTRAINT regions_coordinates
-- FOREIGN KEY (region_id, world_id) REFERENCES regions (id, world_id);

CREATE TABLE regions_forces (
  region_id INTEGER NOT NULL,
  force_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (region_id, force_id, world_id),
  -- FOREIGN KEY (force_id) REFERENCES hf (id),
  FOREIGN KEY (region_id, world_id) REFERENCES regions (id, world_id)
);

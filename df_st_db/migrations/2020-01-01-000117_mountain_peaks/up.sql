
CREATE TABLE mountain_peaks (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  name VARCHAR,
  coord_id INTEGER,
  height INTEGER,
  is_volcano BOOLEAN,

  PRIMARY KEY (id, world_id),
  FOREIGN KEY (coord_id, world_id) REFERENCES coordinates (id, world_id)
);
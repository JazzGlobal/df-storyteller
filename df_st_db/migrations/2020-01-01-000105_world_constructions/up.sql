
CREATE TABLE world_constructions (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  name VARCHAR,
  type VARCHAR,

  PRIMARY KEY (id, world_id)
);

-- ALTER TABLE coordinates
-- ADD CONSTRAINT world_constructions
-- FOREIGN KEY (world_construction_id, world_id)
--   REFERENCES world_constructions (id, world_id);

CREATE TABLE link_he_site (
  he_id INTEGER NOT NULL,
  site_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, site_id, world_id),
  -- FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id),
  FOREIGN KEY (site_id, world_id) REFERENCES historical_figures (id, world_id)
);

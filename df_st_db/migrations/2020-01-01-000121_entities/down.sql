
DROP TABLE entities;
DROP TABLE entity_honors;
DROP TABLE entity_links;
DROP TABLE entity_positions;
DROP TABLE entity_position_assignments;
DROP TABLE entity_occasions;
DROP TABLE entity_occasion_schedules;
DROP TABLE entity_occasion_schedule_features;
DROP TABLE entity_worship_ids;
DROP TABLE entity_weapons;
DROP TABLE entity_professions;
DROP TABLE entity_hf_ids;
DROP TABLE entity_child_en_ids;

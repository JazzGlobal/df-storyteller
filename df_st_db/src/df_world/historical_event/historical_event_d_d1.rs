use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_d_d1;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_d_d1"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventDD1 {
    pub he_id: i32,
    pub world_id: i32,

    pub d_effect: Option<i32>,
    pub d_interaction: Option<i32>,
    pub d_number: Option<i32>,
    pub d_race: Option<i32>,
    pub d_slain: Option<i32>,
    pub d_squad_id: Option<i32>,
    pub d_support_merc_en_id: Option<i32>,
    pub d_tactician_hf_id: Option<i32>,
    pub d_tactics_roll: Option<i32>,
    pub death_penalty: Option<bool>,
    pub defender_civ_id: Option<i32>,
    pub defender_general_hf_id: Option<i32>,
    pub defender_merc_en_id: Option<i32>,
    pub delegated: Option<bool>,
    pub depot_entity_id: Option<i32>,
}

impl HistoricalEventDD1 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventDD1> for HistoricalEventDD1 {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[rustfmt::skip]
    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_d_d1: &[HistoricalEventDD1]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_d_d1::table)
            .values(historical_events_d_d1)
            .on_conflict((
                historical_events_d_d1::he_id,
                historical_events_d_d1::world_id,
            ))
            .do_update()
            .set((
                historical_events_d_d1::d_effect.eq(excluded(historical_events_d_d1::d_effect)),
                historical_events_d_d1::d_interaction.eq(excluded(historical_events_d_d1::d_interaction)),
                historical_events_d_d1::d_number.eq(excluded(historical_events_d_d1::d_number)),
                historical_events_d_d1::d_race.eq(excluded(historical_events_d_d1::d_race)),
                historical_events_d_d1::d_slain.eq(excluded(historical_events_d_d1::d_slain)),
                historical_events_d_d1::d_squad_id.eq(excluded(historical_events_d_d1::d_squad_id)),
                historical_events_d_d1::d_support_merc_en_id.eq(excluded(historical_events_d_d1::d_support_merc_en_id)),
                historical_events_d_d1::d_tactician_hf_id.eq(excluded(historical_events_d_d1::d_tactician_hf_id)),
                historical_events_d_d1::d_tactics_roll.eq(excluded(historical_events_d_d1::d_tactics_roll)),
                historical_events_d_d1::death_penalty.eq(excluded(historical_events_d_d1::death_penalty)),
                historical_events_d_d1::defender_civ_id.eq(excluded(historical_events_d_d1::defender_civ_id)),
                historical_events_d_d1::defender_general_hf_id.eq(excluded(historical_events_d_d1::defender_general_hf_id)),
                historical_events_d_d1::defender_merc_en_id.eq(excluded(historical_events_d_d1::defender_merc_en_id)),
                historical_events_d_d1::delegated.eq(excluded(historical_events_d_d1::delegated)),
                historical_events_d_d1::depot_entity_id.eq(excluded(historical_events_d_d1::depot_entity_id)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_d_d1");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_d_d1: &[HistoricalEventDD1]) {
        diesel::insert_into(historical_events_d_d1::table)
            .values(historical_events_d_d1)
            .execute(conn)
            .expect("Error saving historical_events_d_d1");
    }

    /// Get a list of HistoricalEventDD1 from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventDD1>, Error> {
        /*
        use crate::schema::historical_events_d_d1::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_d_d1.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "d_effect" => d_effect,
                "d_interaction" => d_interaction,
                "d_number" => d_number,
                "d_race" => d_race,
                "d_slain" => d_slain,
                "d_squad_id" => d_squad_id,
                "d_support_merc_en_id" => d_support_merc_en_id,
                "d_tactician_hf_id" => d_tactician_hf_id,
                "d_tactics_roll" => d_tactics_roll,
                "death_penalty" => death_penalty,
                "defender_civ_id" => defender_civ_id,
                "defender_general_hf_id" => defender_general_hf_id,
                "defender_merc_en_id" => defender_merc_en_id,
                "delegated" => delegated,
                "depot_entity_id" => depot_entity_id,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventDD1>, Error> {
        use crate::schema::historical_events_d_d1::dsl::*;
        let query = historical_events_d_d1;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventDD1>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "d_effect",
            "d_interaction",
            "d_number",
            "d_race",
            "d_slain",
            "d_squad_id",
            "d_support_merc_en_id",
            "d_tactician_hf_id",
            "d_tactics_roll",
            "death_penalty",
            "defender_civ_id",
            "defender_general_hf_id",
            "defender_merc_en_id",
            "delegated",
            "depot_entity_id",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventDD1],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_d_d1::dsl::*;
        let query = historical_events_d_d1.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "d_effect" => {d_effect: Option<i32>},
                "d_interaction" => {d_interaction: Option<i32>},
                "d_number" => {d_number: Option<i32>},
                "d_race" => {d_race: Option<i32>},
                "d_slain" => {d_slain: Option<i32>},
                "d_squad_id" => {d_squad_id: Option<i32>},
                "d_support_merc_en_id" => {d_support_merc_en_id: Option<i32>},
                "d_tactician_hf_id" => {d_tactician_hf_id: Option<i32>},
                "d_tactics_roll" => {d_tactics_roll: Option<i32>},
                "death_penalty" => {death_penalty: Option<bool>},
                "defender_civ_id" => {defender_civ_id: Option<i32>},
                "defender_general_hf_id" => {defender_general_hf_id: Option<i32>},
                "defender_merc_en_id" => {defender_merc_en_id: Option<i32>},
                "delegated" => {delegated: Option<bool>},
                "depot_entity_id" => {depot_entity_id: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventDD1, df_st_core::HistoricalEvent> for HistoricalEventDD1 {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.d_effect.add_missing_data(&source.d_effect);
        self.d_interaction.add_missing_data(&source.d_interaction);
        self.d_number.add_missing_data(&source.d_number);
        self.d_race.add_missing_data(&source.d_race);
        self.d_slain.add_missing_data(&source.d_slain);
        self.d_squad_id.add_missing_data(&source.d_squad_id);
        self.d_support_merc_en_id.add_missing_data(&source.d_support_merc_en_id);
        self.d_tactician_hf_id.add_missing_data(&source.d_tactician_hf_id);
        self.d_tactics_roll.add_missing_data(&source.d_tactics_roll);
        self.death_penalty.add_missing_data(&source.death_penalty);
        self.defender_civ_id.add_missing_data(&source.defender_civ_id);
        self.defender_general_hf_id.add_missing_data(&source.defender_general_hf_id);
        self.defender_merc_en_id.add_missing_data(&source.defender_merc_en_id);
        self.delegated.add_missing_data(&source.delegated);
        self.depot_entity_id.add_missing_data(&source.depot_entity_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventDD1> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventDD1) {
        self.id.add_missing_data(&source.he_id);
        self.d_effect.add_missing_data(&source.d_effect);
        self.d_interaction.add_missing_data(&source.d_interaction);
        self.d_number.add_missing_data(&source.d_number);
        self.d_race.add_missing_data(&source.d_race);
        self.d_slain.add_missing_data(&source.d_slain);
        self.d_squad_id.add_missing_data(&source.d_squad_id);
        self.d_support_merc_en_id.add_missing_data(&source.d_support_merc_en_id);
        self.d_tactician_hf_id.add_missing_data(&source.d_tactician_hf_id);
        self.d_tactics_roll.add_missing_data(&source.d_tactics_roll);
        self.death_penalty.add_missing_data(&source.death_penalty);
        self.defender_civ_id.add_missing_data(&source.defender_civ_id);
        self.defender_general_hf_id.add_missing_data(&source.defender_general_hf_id);
        self.defender_merc_en_id.add_missing_data(&source.defender_merc_en_id);
        self.delegated.add_missing_data(&source.delegated);
        self.depot_entity_id.add_missing_data(&source.depot_entity_id);
    }
}

impl PartialEq for HistoricalEventDD1 {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventDD1 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventDD1> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventDD1) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventDD1 {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}

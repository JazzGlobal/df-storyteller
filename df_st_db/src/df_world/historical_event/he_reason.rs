use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::he_reasons;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "he_reasons"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HEReason {
    pub he_id: i32,
    pub world_id: i32,

    pub type_: Option<String>,
    pub glorify_hf_id: Option<i32>,
    pub artifact_is_heirloom_of_family_hf_id: Option<i32>,
    pub artifact_is_symbol_of_entity_position: Option<i32>,
}

impl HEReason {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HEReason, HEReason> for HEReason {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, he_reasons: &[HEReason]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(he_reasons::table)
            .values(he_reasons)
            .on_conflict((he_reasons::he_id, he_reasons::world_id))
            .do_update()
            .set((
                he_reasons::type_.eq(excluded(he_reasons::type_)),
                he_reasons::glorify_hf_id.eq(excluded(he_reasons::glorify_hf_id)),
                he_reasons::artifact_is_heirloom_of_family_hf_id
                    .eq(excluded(he_reasons::artifact_is_heirloom_of_family_hf_id)),
                he_reasons::artifact_is_symbol_of_entity_position
                    .eq(excluded(he_reasons::artifact_is_symbol_of_entity_position)),
            ))
            .execute(conn)
            .expect("Error saving he_reasons");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, he_reasons: &[HEReason]) {
        diesel::insert_into(he_reasons::table)
            .values(he_reasons)
            .execute(conn)
            .expect("Error saving he_reasons");
    }

    /// Get a list of HEReason from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HEReason>, Error> {
        use crate::schema::he_reasons::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = he_reasons.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "type" => type_,
                "glorify_hf_id" => glorify_hf_id,
                "artifact_is_heirloom_of_family_hf_id" => artifact_is_heirloom_of_family_hf_id,
                "artifact_is_symbol_of_entity_position" => artifact_is_symbol_of_entity_position,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HEReason>, Error> {
        use crate::schema::he_reasons::dsl::*;
        let query = he_reasons;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HEReason>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "type",
            "glorify_hf_id",
            "artifact_is_heirloom_of_family_hf_id",
            "artifact_is_symbol_of_entity_position",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HEReason],
        core_list: Vec<df_st_core::HEReason>,
    ) -> Result<Vec<df_st_core::HEReason>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::he_reasons::dsl::*;
        let query = he_reasons.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "type" => {type_: Option<String>},
                "glorify_hf_id" => {glorify_hf_id: Option<i32>},
                "artifact_is_heirloom_of_family_hf_id" => {artifact_is_heirloom_of_family_hf_id: Option<i32>},
                "artifact_is_symbol_of_entity_position" => {artifact_is_symbol_of_entity_position: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HEReason, df_st_core::HEReason> for HEReason {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HEReason) {
        self.type_.add_missing_data(&source.type_);
        self.glorify_hf_id.add_missing_data(&source.glorify_hf_id);
        self.artifact_is_heirloom_of_family_hf_id.add_missing_data(&source.artifact_is_heirloom_of_family_hf_id);
        self.artifact_is_symbol_of_entity_position.add_missing_data(&source.artifact_is_symbol_of_entity_position);
    }
}

/// From DB to Core
impl Filler<df_st_core::HEReason, HEReason> for df_st_core::HEReason {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HEReason) {
        self.type_.add_missing_data(&source.type_);
        self.glorify_hf_id.add_missing_data(&source.glorify_hf_id);
        self.artifact_is_heirloom_of_family_hf_id.add_missing_data(&source.artifact_is_heirloom_of_family_hf_id);
        self.artifact_is_symbol_of_entity_position.add_missing_data(&source.artifact_is_symbol_of_entity_position);
    }
}
impl Filler<df_st_core::HistoricalEvent, HEReason> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HEReason) {
        self.reason_obj.add_missing_data(&Some(source.clone()));
    }
}

impl PartialEq for HEReason {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HEReason {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HEReason> for df_st_core::HEReason {
    fn eq(&self, other: &HEReason) -> bool {
        self.type_ == other.type_
    }
}

impl PartialEq<df_st_core::HEReason> for HEReason {
    fn eq(&self, other: &df_st_core::HEReason) -> bool {
        self.type_ == other.type_
    }
}

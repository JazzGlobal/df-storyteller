use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_occasions;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_occasions"]
#[primary_key(en_id, local_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityOccasion {
    pub en_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub event: Option<i32>,
}

impl EntityOccasion {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::EntityOccasion, EntityOccasion> for EntityOccasion {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_occasions: &[EntityOccasion]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_occasions::table)
            .values(entity_occasions)
            .on_conflict((
                entity_occasions::en_id,
                entity_occasions::local_id,
                entity_occasions::world_id,
            ))
            .do_update()
            .set((
                entity_occasions::name.eq(excluded(entity_occasions::name)),
                entity_occasions::event.eq(excluded(entity_occasions::event)),
            ))
            .execute(conn)
            .expect("Error saving entity_occasions");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_occasions: &[EntityOccasion]) {
        diesel::insert_into(entity_occasions::table)
            .values(entity_occasions)
            .execute(conn)
            .expect("Error saving entity_occasions");
    }

    /// Get a list of EntityOccasion from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityOccasion>, Error> {
        use crate::schema::entity_occasions::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_occasions.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "local_id" => local_id,
                "name" => name,
                "event" => event,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityOccasion>, Error> {
        use crate::schema::entity_occasions::dsl::*;
        let query = entity_occasions;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        let query = query.filter(local_id.eq(id_filter.get("local_id").unwrap_or(&0)));
        Ok(query.first::<EntityOccasion>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["en_id", "local_id", "name", "event"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityOccasion],
        core_list: Vec<df_st_core::EntityOccasion>,
    ) -> Result<Vec<df_st_core::EntityOccasion>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_occasions::dsl::*;
        let query = entity_occasions.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "local_id" => {local_id: i32},
                "name" => {name: Option<String>},
                "event" => {event: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<EntityOccasion, df_st_core::EntityOccasion> for EntityOccasion {
    fn add_missing_data(&mut self, source: &df_st_core::EntityOccasion) {
        self.local_id.add_missing_data(&source.local_id);
        self.name.add_missing_data(&source.name);
        self.event.add_missing_data(&source.event);
    }
}

/// From DB to Core
impl Filler<df_st_core::EntityOccasion, EntityOccasion> for df_st_core::EntityOccasion {
    fn add_missing_data(&mut self, source: &EntityOccasion) {
        self.local_id.add_missing_data(&source.local_id);
        self.name.add_missing_data(&source.name);
        self.event.add_missing_data(&source.event);
    }
}

impl PartialEq for EntityOccasion {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.local_id == other.local_id
    }
}

impl Hash for EntityOccasion {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq<EntityOccasion> for df_st_core::EntityOccasion {
    fn eq(&self, other: &EntityOccasion) -> bool {
        self.local_id == other.local_id
    }
}

impl PartialEq<df_st_core::EntityOccasion> for EntityOccasion {
    fn eq(&self, other: &df_st_core::EntityOccasion) -> bool {
        self.local_id == other.local_id
    }
}

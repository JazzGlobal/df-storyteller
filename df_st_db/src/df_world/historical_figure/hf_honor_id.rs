use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_honor_ids;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hf_honor_ids"]
#[primary_key(hf_id, entity_id, honor_id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFHonorID {
    pub hf_id: i32,
    pub entity_id: i32,
    pub honor_id: i32,
    pub world_id: i32,
}

impl HFHonorID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HFHonorID, HFHonorID> for HFHonorID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_honor_ids: &[HFHonorID]) {
        diesel::insert_into(hf_honor_ids::table)
            .values(hf_honor_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hf_honor_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_honor_ids: &[HFHonorID]) {
        diesel::insert_into(hf_honor_ids::table)
            .values(hf_honor_ids)
            .execute(conn)
            .expect("Error saving hf_honor_ids");
    }

    /// Get a list of HFHonorID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HFHonorID>, Error> {
        use crate::schema::hf_honor_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_honor_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "honor_id" => honor_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "honor_id" => honor_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFHonorID>, Error> {
        use crate::schema::hf_honor_ids::dsl::*;
        let query = hf_honor_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        let query = query.filter(entity_id.eq(id_filter.get("entity_id").unwrap_or(&0)));
        let query = query.filter(honor_id.eq(id_filter.get("honor_id").unwrap_or(&0)));
        Ok(query.first::<HFHonorID>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["hf_id", "entity_id", "honor_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFHonorID],
        core_list: Vec<HFHonorID>,
    ) -> Result<Vec<HFHonorID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_honor_ids::dsl::*;
        let query = hf_honor_ids.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "honor_id" => honor_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "entity_id" => {entity_id: i32},
                "honor_id" => {honor_id: i32},
            };},
        };
    }
}

impl PartialEq for HFHonorID {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.entity_id == other.entity_id
            && self.honor_id == other.honor_id
    }
}

impl Hash for HFHonorID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
        self.honor_id.hash(state);
    }
}

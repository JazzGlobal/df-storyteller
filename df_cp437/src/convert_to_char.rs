/// Convert an array of bytes encoded as CP437 into a byte array encoded as UTF-8.
pub fn convert_cp437_to_utf8_char(input_byte: u8) -> Result<char, String> {
    // Make output at least the size of input
    let mut output_bytes: Vec<u8> = Vec::with_capacity(3);

    let new_char = crate::convert_cp437_byte_to_utf8_bytes_for_chars(&input_byte);
    if new_char[0] != 0x00 {
        output_bytes.push(new_char[0]);
    }
    if new_char[1] != 0x00 {
        output_bytes.push(new_char[1]);
    }
    output_bytes.push(new_char[2]);

    let string_value =
        String::from_utf8(output_bytes).expect("Char is a non UTF-8 character, `df_cp437` bug.");
    let chars: Vec<char> = string_value.chars().collect();
    if chars.len() != 1 {
        return Err("String len should be exactly 1 character".to_owned());
    }
    Ok(*chars.first().unwrap())
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_char_convert() {
        let input = 0x12;
        let output = '↕';

        let result = crate::convert_cp437_to_utf8_char(input).unwrap();
        assert_eq!(result, output);
    }
}

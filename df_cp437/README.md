# CP437 to UTF-8
[![unsafe forbidden](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)

This project is part of [DF Storyteller](https://gitlab.com/df_storyteller/df-storyteller).
The project is inspired by the [CP437](https://github.com/timglabisch/rust_cp437) crate. 
Although everything had been changed since that point.

Note that the code page correspondence with the code page used in 
[Dwarf Fortress](https://www.bay12games.com/dwarves/), but can be used for other applications.

It allows buffered reads of files that use CP437 and provides an output in UTF-8.
This can be used together with [Serde](https://github.com/serde-rs/serde).

It also provides an interface for a progress bar to be added.

## License

`df_cp437` is distributed under the terms of both the MIT license and 
the Apache License (Version 2.0).
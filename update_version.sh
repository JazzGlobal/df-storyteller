#!/bin/bash

# This tool is for updating all the version numbers in 1 go

# New version that should be inserted
# Always use [Semantic Versioning numbers](https://semver.org/)
# Examples: 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-alpha.beta < 1.0.0-beta < 1.0.0-beta.2 
#           < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0
NEW_VER="0.3.0-development-3"

# List of all the files to update
FILE=(
  "./df_storyteller/Cargo.toml"
  "./df_st_api/Cargo.toml"
  "./df_st_cli/Cargo.toml"
  "./df_st_core/Cargo.toml"
  "./df_st_db/Cargo.toml"
  "./df_st_derive/Cargo.toml"
  "./df_st_guide/Cargo.toml"
  "./df_st_image_maps/Cargo.toml"
  "./df_st_image_site_maps/Cargo.toml"
  "./df_st_legends/Cargo.toml"
  "./df_st_legends_plus/Cargo.toml"
  "./df_st_parser/Cargo.toml"
  "./df_st_updater/Cargo.toml"
  "./df_st_world_history/Cargo.toml"
  "./df_st_world_sites_and_pops/Cargo.toml"
)

# This code asumes you have `sd` installed:
# https://github.com/chmln/sd
# `cargo install sd`

for file in ${FILE[@]}
do
  sd "version = \"([0-9.a-z-])+\" #:version" "version = \"$NEW_VER\" #:version" $file
done

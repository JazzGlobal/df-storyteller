#!/bin/bash

# This command assumes you have Cargo audit installed:
# https://github.com/RustSec/cargo-audit
# `cargo install cargo-audit`

for file in `ls ./*.lock`
do
    echo "---- Checking: $file ----"
    cargo audit -q -f $file
done
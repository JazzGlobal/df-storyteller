pub fn caste_string_to_int(caste_label: String) -> i32 {
    let mut caste = caste_label;
    caste.make_ascii_lowercase();
    match caste.as_ref() {
        "female" => 0,
        "male" => 1,
        "default" => -1,
        // TODO: "worker" see r26
        x => {
            log::warn!("Conversation Function: Caste could not match: {}", x);
            -1
        }
    }
}

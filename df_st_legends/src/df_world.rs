use df_st_core::{Filler, HasUnknown};
use df_st_derive::HasUnknown;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

mod artifacts;
mod dance_forms;
mod entities;
mod entity_populations;
mod historical_eras;
mod historical_event_collections;
mod historical_events;
mod historical_figures;
mod musical_forms;
mod poetic_forms;
mod regions;
mod sites;
mod underground_regions;
mod world_constructions;
mod written_contents;

pub use artifacts::{Artifact, Artifacts};
pub use dance_forms::{DanceForm, DanceForms};
pub use entities::{Entities, Entity};
pub use entity_populations::{EntityPopulation, EntityPopulations};
pub use historical_eras::{HistoricalEra, HistoricalEras};
pub use historical_event_collections::{HistoricalEventCollection, HistoricalEventCollections};
pub use historical_events::{HistoricalEvent, HistoricalEvents};
pub use historical_figures::{HistoricalFigure, HistoricalFigures};
pub use musical_forms::{MusicalForm, MusicalForms};
pub use poetic_forms::{PoeticForm, PoeticForms};
pub use regions::{Region, Regions};
pub use sites::{Site, Sites};
pub use underground_regions::{UndergroundRegion, UndergroundRegions};
pub use world_constructions::{WorldConstruction, WorldConstructions};
pub use written_contents::{WrittenContent, WrittenContents};

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, Default)]
pub struct DFWorldLegends {
    pub regions: Option<Regions>,
    pub underground_regions: Option<UndergroundRegions>,
    pub sites: Option<Sites>,
    pub world_constructions: Option<WorldConstructions>,
    pub artifacts: Option<Artifacts>,
    pub historical_figures: Option<HistoricalFigures>,
    pub entity_populations: Option<EntityPopulations>,
    pub entities: Option<Entities>,
    pub historical_events: Option<HistoricalEvents>,
    pub historical_event_collections: Option<HistoricalEventCollections>,
    pub historical_eras: Option<HistoricalEras>,
    pub written_contents: Option<WrittenContents>,
    pub poetic_forms: Option<PoeticForms>,
    pub musical_forms: Option<MusicalForms>,
    pub dance_forms: Option<DanceForms>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl DFWorldLegends {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Filler<df_st_core::DFWorld, DFWorldLegends> for df_st_core::DFWorld {
    fn add_missing_data(&mut self, source: &DFWorldLegends) {
        if let Some(regions) = &source.regions {
            self.regions.add_missing_data(regions);
        }
        if let Some(underground_regions) = &source.underground_regions {
            self.underground_regions
                .add_missing_data(underground_regions);
        }
        if let Some(sites) = &source.sites {
            self.sites.add_missing_data(sites);
        }
        if let Some(artifacts) = &source.artifacts {
            self.artifacts.add_missing_data(artifacts);
        }
        if let Some(world_constructions) = &source.world_constructions {
            self.world_constructions
                .add_missing_data(world_constructions);
        }
        if let Some(entity_populations) = &source.entity_populations {
            self.entity_populations.add_missing_data(entity_populations);
        }
        if let Some(historical_figures) = &source.historical_figures {
            self.historical_figures.add_missing_data(historical_figures);
        }
        if let Some(poetic_forms) = &source.poetic_forms {
            self.poetic_forms.add_missing_data(poetic_forms);
        }
        if let Some(musical_forms) = &source.musical_forms {
            self.musical_forms.add_missing_data(musical_forms);
        }
        if let Some(dance_forms) = &source.dance_forms {
            self.dance_forms.add_missing_data(dance_forms);
        }
        if let Some(written_contents) = &source.written_contents {
            self.written_contents.add_missing_data(written_contents);
        }
        if let Some(historical_eras) = &source.historical_eras {
            self.historical_eras.add_missing_data(historical_eras);
        }
        if let Some(entities) = &source.entities {
            self.entities.add_missing_data(entities);
        }
        if let Some(historical_events) = &source.historical_events {
            self.historical_events.add_missing_data(historical_events);
        }
        if let Some(historical_event_collections) = &source.historical_event_collections {
            self.historical_event_collections
                .add_missing_data(historical_event_collections);
        }
    }
}

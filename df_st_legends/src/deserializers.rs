use df_st_core::{Coordinate, Rectangle};
use regex::Regex;
use serde::{Deserialize, Deserializer};

pub fn coordinate_deserializer<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<Coordinate>, D::Error> {
    let coords: String = Deserialize::deserialize(deserializer)?;
    if coords.is_empty() {
        return Ok(None);
    }
    let re = Regex::new(r"^(-?[0-9]+),(-?[0-9]+)$").unwrap();
    let values = match re.captures(&coords) {
        Some(v) => v,
        None => return Ok(None),
    };
    Ok(Some(Coordinate {
        x: values[1].parse::<i32>().unwrap_or(-1),
        y: values[2].parse::<i32>().unwrap_or(-1),
        ..Default::default()
    }))
}

pub fn rectangle_deserializer<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<Rectangle>, D::Error> {
    let rectangle: String = Deserialize::deserialize(deserializer)?;
    if rectangle.is_empty() {
        return Ok(None);
    }
    let re = Regex::new(r"^(-?[0-9]+),(-?[0-9]+):(-?[0-9]+),(-?[0-9]+)$").unwrap();
    let values = match re.captures(&rectangle) {
        Some(v) => v,
        None => return Ok(None),
    };
    Ok(Some(Rectangle {
        x1: values[1].parse::<i32>().unwrap_or(-1),
        y1: values[2].parse::<i32>().unwrap_or(-1),
        x2: values[3].parse::<i32>().unwrap_or(-1),
        y2: values[4].parse::<i32>().unwrap_or(-1),
        ..Default::default()
    }))
}

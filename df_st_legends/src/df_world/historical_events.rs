use crate::converters::*;
use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;
// use crate::deserializers::{coordinate_deserializer};

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct HistoricalEvent {
    pub id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub year: Option<i32>,
    pub seconds72: Option<i32>,

    // All others are optional
    pub a_hfid: Option<Vec<i32>>,
    pub a_leader_hfid: Option<i32>,
    pub a_leadership_roll: Option<i32>,
    pub a_squad_id: Option<i32>,
    pub a_support_merc_enid: Option<i32>,
    pub a_tactician_hfid: Option<i32>,
    pub a_tactics_roll: Option<i32>,
    pub abandoned: Option<()>,
    pub account_shift: Option<i32>,
    pub acquirer_enid: Option<i32>,
    pub acquirer_hfid: Option<i32>,
    pub action: Option<String>,
    pub actor_hfid: Option<i32>,
    pub agreement_id: Option<i32>,
    pub allotment: Option<i32>,
    pub allotment_index: Option<i32>,
    pub ally_defense_bonus: Option<i32>,
    pub appointer_hfid: Option<i32>,
    pub arresting_enid: Option<i32>,
    pub artifact_id: Option<i32>,
    pub attacker_civ_id: Option<i32>,
    pub attacker_general_hfid: Option<i32>,
    pub attacker_hfid: Option<i32>,
    pub attacker_merc_enid: Option<i32>,

    pub body_state: Option<String>,
    pub builder_hfid: Option<i32>,
    pub building_profile_id: Option<i32>,

    pub cause: Option<String>,
    pub changee_hfid: Option<i32>,
    pub changer_hfid: Option<i32>,
    pub circumstance: Option<String>,
    pub circumstance_id: Option<i32>,
    pub civ_entity_id: Option<i32>,
    pub civ_id: Option<i32>,
    pub claim: Option<String>,
    pub coconspirator_bonus: Option<i32>,
    pub coconspirator_hfid: Option<i32>,
    pub competitor_hfid: Option<Vec<i32>>,
    pub confessed_after_apb_arrest_enid: Option<i32>,
    pub conspirator_hfid: Option<Vec<i32>>,
    pub contact_hfid: Option<i32>,
    pub contacted_enid: Option<i32>,
    pub contactor_enid: Option<i32>,
    pub convict_is_contact: Option<()>,
    pub convicted_hfid: Option<i32>,
    pub convicter_enid: Option<i32>,
    //#[serde(deserialize_with = "coordinate_deserializer")]
    // #[serde(default)]
    //pub coords: Option<Coordinate>,
    // TODO implement deserialize_with in DeserializeBestEffort
    pub coords: Option<String>,
    pub corrupt_convicter_hfid: Option<i32>,
    pub corruptor_hfid: Option<i32>,
    pub corruptor_identity: Option<i32>,
    pub corruptor_seen_as: Option<String>,
    pub creator_hfid: Option<i32>,
    pub crime: Option<String>,

    pub d_effect: Option<i32>,
    pub d_hfid: Vec<i32>,
    pub d_interaction: Option<i32>,
    pub d_number: Option<i32>,
    pub d_race: Option<i32>,
    pub d_slain: Option<i32>,
    pub d_squad_id: Option<i32>,
    pub d_support_merc_enid: Option<i32>,
    pub d_tactician_hfid: Option<i32>,
    pub d_tactics_roll: Option<i32>,
    pub death_penalty: Option<()>,
    pub defender_civ_id: Option<i32>,
    pub defender_general_hfid: Option<i32>,
    pub defender_merc_enid: Option<i32>,
    pub delegated: Option<()>,
    pub depot_entity_id: Option<i32>,
    pub dest_entity_id: Option<i32>,
    pub dest_site_id: Option<i32>,
    pub dest_structure_id: Option<i32>,
    pub destroyed_structure_id: Option<i32>,
    pub destroyer_enid: Option<i32>,
    pub detected: Option<()>,
    pub did_not_reveal_all_in_interrogation: Option<()>,
    pub disturbance: Option<()>,
    pub dispute: Option<String>,
    pub doer_hfid: Option<i32>,

    pub enslaved_hfid: Option<i32>,
    pub entity_1: Option<i32>,
    pub entity_2: Option<i32>,
    pub entity_id: Option<i32>,
    pub entity_id_1: Option<i32>,
    pub entity_id_2: Option<i32>,
    pub exiled: Option<()>,
    pub expelled_creature: Option<Vec<i32>>,
    pub expelled_hfid: Option<Vec<i32>>,
    pub expelled_number: Option<Vec<i32>>,
    pub expelled_pop_id: Option<Vec<i32>>,

    pub failed_judgment_test: Option<()>,
    pub feature_layer_id: Option<i32>,
    pub first: Option<()>,
    pub fled_civ_id: Option<i32>,
    pub fooled_hfid: Option<i32>,
    pub form_id: Option<i32>,
    pub framer_hfid: Option<i32>,
    pub from_original: Option<()>,

    pub gambler_hfid: Option<i32>,
    pub ghost: Option<String>,
    pub giver_entity_id: Option<i32>,
    pub giver_hist_figure_id: Option<i32>,
    pub group_1_hfid: Option<i32>,
    pub group_2_hfid: Option<i32>,
    pub group_hfid: Option<i32>,

    pub hardship: Option<()>,
    pub held_firm_in_interrogation: Option<()>,
    pub hf_rep_1_of_2: Option<String>,
    pub hf_rep_2_of_1: Option<String>,
    pub hfid: Option<i32>,
    pub hfid1: Option<i32>,
    pub hfid2: Option<i32>,
    pub hfid_target: Option<i32>,
    pub hist_fig_id: Option<i32>,
    pub hist_figure_id: Option<i32>,
    pub honor_id: Option<i32>,

    pub identity_id: Option<i32>,
    pub identity_id1: Option<i32>,
    pub identity_id2: Option<i32>,
    pub implicated_hfid: Option<Vec<i32>>,
    pub inherited: Option<()>,
    pub initiating_enid: Option<i32>,
    pub instigator_hfid: Option<i32>,
    pub interaction: Option<String>,
    pub interrogator_hfid: Option<i32>,

    pub join_entity_id: Option<i32>,
    pub joined_entity_id: Option<i32>,
    pub joiner_entity_id: Option<i32>,
    pub joining_enid: Option<Vec<i32>>,

    pub knowledge: Option<String>, // TODO Parse further
    pub last_owner_hfid: Option<i32>,
    pub law_add: Option<String>,
    pub law_remove: Option<String>,
    pub leader_hfid: Option<i32>,
    pub leaver_civ_id: Option<i32>,
    pub link: Option<String>,
    pub lost_value: Option<()>,
    pub lure_hfid: Option<i32>,

    pub master_wcid: Option<i32>,
    pub method: Option<String>,
    pub modification: Option<String>,
    pub modifier_hfid: Option<i32>,
    pub mood: Option<String>,
    pub moved_to_site_id: Option<i32>,

    pub name_only: Option<()>,
    pub new_ab_id: Option<i32>,
    pub new_account: Option<i32>,
    pub new_artifact_id: Option<i32>,
    pub new_caste: Option<String>,
    pub new_equipment_level: Option<i32>,
    pub new_leader_hfid: Option<i32>,
    pub new_race: Option<String>,
    pub new_site_civ_id: Option<i32>,
    pub no_defeat_mention: Option<()>,
    pub no_prison_available: Option<()>,

    pub occasion_id: Option<i32>,
    pub old_ab_id: Option<i32>,
    pub old_account: Option<i32>,
    pub old_artifact_id: Option<i32>,
    pub old_caste: Option<String>,
    pub old_race: Option<String>,
    pub outcome: Option<String>,
    pub overthrown_hfid: Option<i32>,

    pub partial_incorporation: Option<()>,
    pub payer_entity_id: Option<i32>,
    pub payer_hfid: Option<i32>,
    pub persecutor_enid: Option<i32>,
    pub persecutor_hfid: Option<i32>,
    pub plotter_hfid: Option<i32>,
    pub pop_flid: Option<i32>,
    pub pop_number_moved: Option<i32>,
    pub pop_race: Option<i32>,
    pub pop_srid: Option<i32>,
    pub pos_taker_hfid: Option<i32>,
    pub position_id: Option<i32>,
    pub position_profile_id: Option<i32>,
    pub prison_months: Option<i32>,
    pub production_zone_id: Option<i32>,
    pub promise_to_hfid: Option<i32>,
    pub property_confiscated_from_hfid: Option<i32>,
    pub purchased_unowned: Option<()>,

    pub quality: Option<i32>,
    pub rampage_civ_id: Option<i32>,
    pub ransomed_hfid: Option<i32>,
    pub ransomer_hfid: Option<i32>,
    pub reason: Option<String>,
    pub reason_id: Option<i32>,
    pub rebuilt: Option<()>,
    pub rebuilt_ruined: Option<()>,
    pub receiver_entity_id: Option<i32>,
    pub receiver_hist_figure_id: Option<i32>,
    pub relationship: Option<String>,
    pub relevant_entity_id: Option<i32>,
    pub relevant_id_for_method: Option<i32>,
    pub relevant_position_profile_id: Option<i32>,
    pub religion_id: Option<i32>,
    pub resident_civ_id: Option<i32>,
    pub result: Option<String>,
    #[serde(alias = "return")]
    pub return_: Option<()>,

    pub saboteur_hfid: Option<i32>,
    pub schedule_id: Option<i32>,
    pub searcher_civ_id: Option<i32>,
    pub season: Option<String>,
    pub secret_goal: Option<String>,
    pub seeker_hfid: Option<i32>,
    pub seller_hfid: Option<i32>,
    pub shrine_amount_destroyed: Option<i32>,
    pub site_civ_id: Option<i32>,
    pub site_entity_id: Option<i32>,
    pub site_hfid: Option<i32>,
    pub site_id: Option<i32>,
    pub site_id1: Option<i32>,
    pub site_id2: Option<i32>,
    pub site_id_1: Option<i32>,
    pub site_id_2: Option<i32>,
    pub site_property_id: Option<i32>,
    pub situation: Option<String>,
    pub skill_at_time: Option<i32>,
    pub slayer_caste: Option<String>,
    pub slayer_hfid: Option<i32>,
    pub slayer_item_id: Option<i32>,
    pub slayer_race: Option<String>,
    pub slayer_shooter_item_id: Option<i32>,
    pub snatcher_hfid: Option<i32>,
    pub source_entity_id: Option<i32>,
    pub source_site_id: Option<i32>,
    pub source_structure_id: Option<i32>,
    pub speaker_hfid: Option<i32>,
    pub spotter_hfid: Option<i32>,
    pub start: Option<()>,
    pub state: Option<String>,
    pub structure_id: Option<i32>,
    pub student_hfid: Option<i32>,
    pub subregion_id: Option<i32>,
    pub subtype: Option<String>,
    pub successful: Option<()>,
    pub surveiled_coconspirator: Option<()>,
    pub surveiled_contact: Option<()>,
    pub surveiled_convicted: Option<()>,
    pub surveiled_target: Option<()>,

    pub target_civ_id: Option<i32>,
    pub target_enid: Option<i32>,
    pub target_hfid: Option<i32>,
    pub target_identity: Option<i32>,
    pub target_seen_as: Option<String>,
    pub teacher_hfid: Option<i32>,
    pub took_items: Option<()>,
    pub took_livestock: Option<()>,
    pub top_facet: Option<String>,
    pub top_facet_modifier: Option<i32>,
    pub top_facet_rating: Option<i32>,
    pub top_relationship_factor: Option<String>,
    pub top_relationship_modifier: Option<i32>,
    pub top_relationship_rating: Option<i32>,
    pub top_value: Option<String>,
    pub top_value_modifier: Option<i32>,
    pub top_value_rating: Option<i32>,
    pub topic: Option<String>,
    pub trader_entity_id: Option<i32>,
    pub trader_hfid: Option<i32>,
    pub trickster_hfid: Option<i32>,

    pub unit_id: Option<i32>,
    pub unit_type: Option<String>,
    pub unretire: Option<()>,

    pub wanted_and_recognized: Option<()>,
    pub was_raid: Option<()>,
    pub was_torture: Option<()>,
    pub wc_id: Option<i32>,
    pub wcid: Option<i32>,
    pub winner_hfid: Option<i32>,
    pub woundee_hfid: Option<i32>,
    pub wounder_hfid: Option<i32>,
    pub wrongful_conviction: Option<()>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HistoricalEvents {
    pub historical_event: Option<Vec<HistoricalEvent>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalEvent, HistoricalEvent> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEvent) {
        self.id.add_missing_data(&source.id);
        let mut source_type = source.type_.clone();
        if let Some(type_) = &source.type_ {
            source_type = Some(type_.replace(' ', "_"));
        }
        self.type_.add_missing_data(&source_type);
        self.year.add_missing_data(&source.year);
        self.seconds72.add_missing_data(&source.seconds72);

        self.a_hf_ids.add_missing_data(&source.a_hfid);
        self.a_leader_hf_id.add_missing_data(&source.a_leader_hfid);
        self.a_leadership_roll.add_missing_data(&source.a_leadership_roll);
        self.a_squad_id.add_missing_data(&source.a_squad_id);
        self.a_support_merc_en_id.add_missing_data(&source.a_support_merc_enid);
        self.a_tactician_hf_id.add_missing_data(&source.a_tactician_hfid);
        self.a_tactics_roll.add_missing_data(&source.a_tactics_roll);
        self.abandoned.add_missing_data(&source.abandoned);
        self.account_shift.add_missing_data(&source.account_shift);
        self.acquirer_en_id.add_missing_data(&source.acquirer_enid);
        self.acquirer_hf_id.add_missing_data(&source.acquirer_hfid);
        self.action.add_missing_data(&source.action);
        self.actor_hf_id.add_missing_data(&source.actor_hfid);
        self.agreement_id.add_missing_data(&source.agreement_id);
        self.allotment.add_missing_data(&source.allotment);
        self.allotment_index.add_missing_data(&source.allotment_index);
        self.ally_defense_bonus.add_missing_data(&source.ally_defense_bonus);
        self.appointer_hf_id.add_missing_data(&source.appointer_hfid);
        self.arresting_en_id.add_missing_data(&source.arresting_enid);
        self.artifact_id.add_missing_data(&source.artifact_id);
        self.attacker_civ_id.add_missing_data(&source.attacker_civ_id);
        self.attacker_general_hf_id.add_missing_data(&source.attacker_general_hfid);
        self.attacker_hf_id.add_missing_data(&source.attacker_hfid);
        self.attacker_merc_en_id.add_missing_data(&source.attacker_merc_enid);

        self.body_state.add_missing_data(&source.body_state);
        self.builder_hf_id.add_missing_data(&source.builder_hfid);
        self.building_profile_id.add_missing_data(&source.building_profile_id);

        self.cause.add_missing_data(&source.cause);
        self.changee_hf_id.add_missing_data(&source.changee_hfid);
        self.changer_hf_id.add_missing_data(&source.changer_hfid);
        self.circumstance.add_missing_data(&source.circumstance);
        self.circumstance_id.add_missing_data(&source.circumstance_id);
        self.civ_entity_id.add_missing_data(&source.civ_entity_id);
        self.civ_id.add_missing_data(&source.civ_id);
        self.claim.add_missing_data(&source.claim);
        self.coconspirator_bonus.add_missing_data(&source.coconspirator_bonus);
        self.coconspirator_hf_id.add_missing_data(&source.coconspirator_hfid);
        self.competitor_hf_id.add_missing_data(&source.competitor_hfid);
        self.confessed_after_apb_arrest_en_id.add_missing_data(&source.confessed_after_apb_arrest_enid);
        self.conspirator_hf_id.add_missing_data(&source.conspirator_hfid);
        self.contact_hf_id.add_missing_data(&source.contact_hfid);
        self.contacted_en_id.add_missing_data(&source.contacted_enid);
        self.contactor_en_id.add_missing_data(&source.contactor_enid);
        self.convict_is_contact.add_missing_data(&source.convict_is_contact);
        self.convicted_hf_id.add_missing_data(&source.convicted_hfid);
        self.convicter_en_id.add_missing_data(&source.convicter_enid);
        self.coord.add_missing_data(&source.coords);
        self.corrupt_convicter_hf_id.add_missing_data(&source.corrupt_convicter_hfid);
        self.corruptor_hf_id.add_missing_data(&source.corruptor_hfid);
        self.corruptor_identity.add_missing_data(&source.corruptor_identity);
        self.corruptor_seen_as.add_missing_data(&source.corruptor_seen_as);
        self.creator_hf_id.add_missing_data(&source.creator_hfid);
        self.crime.add_missing_data(&source.crime);

        self.d_effect.add_missing_data(&source.d_effect);
        self.d_hf_ids.add_missing_data(&source.d_hfid);
        self.d_interaction.add_missing_data(&source.d_interaction);
        self.d_number.add_missing_data(&source.d_number);
        self.d_race.add_missing_data(&source.d_race);
        self.d_slain.add_missing_data(&source.d_slain);
        self.d_squad_id.add_missing_data(&source.d_squad_id);
        self.d_support_merc_en_id.add_missing_data(&source.d_support_merc_enid);
        self.d_tactician_hf_id.add_missing_data(&source.d_tactician_hfid);
        self.d_tactics_roll.add_missing_data(&source.d_tactics_roll);
        self.death_penalty.add_missing_data(&source.death_penalty);
        self.defender_civ_id.add_missing_data(&source.defender_civ_id);
        self.defender_general_hf_id.add_missing_data(&source.defender_general_hfid);
        self.defender_merc_en_id.add_missing_data(&source.defender_merc_enid);
        self.delegated.add_missing_data(&source.delegated);
        self.depot_entity_id.add_missing_data(&source.depot_entity_id);
        self.dest_entity_id.add_missing_data(&source.dest_entity_id);
        self.dest_site_id.add_missing_data(&source.dest_site_id);
        self.dest_structure_id.add_missing_data(&source.dest_structure_id);
        self.destroyed_structure_id.add_missing_data(&source.destroyed_structure_id);
        self.destroyer_en_id.add_missing_data(&source.destroyer_enid);
        self.detected.add_missing_data(&source.detected);
        self.did_not_reveal_all_in_interrogation.add_missing_data(&source.did_not_reveal_all_in_interrogation);
        self.disturbance.add_missing_data(&source.disturbance);
        self.dispute.add_missing_data(&source.dispute);
        self.doer_hf_id.add_missing_data(&source.doer_hfid);

        self.enslaved_hf_id.add_missing_data(&source.enslaved_hfid);
        self.entity_id_1.add_missing_data(&source.entity_1);
        self.entity_id_2.add_missing_data(&source.entity_2);
        self.entity_id.add_missing_data(&source.entity_id);
        self.entity_id_1.add_missing_data(&source.entity_id_1);
        self.entity_id_2.add_missing_data(&source.entity_id_2);
        self.exiled.add_missing_data(&source.exiled);
        self.expelled_creature.add_missing_data(&source.expelled_creature);
        self.expelled_hf_id.add_missing_data(&source.expelled_hfid);
        self.expelled_number.add_missing_data(&source.expelled_number);
        self.expelled_pop_id.add_missing_data(&source.expelled_pop_id);

        self.failed_judgment_test.add_missing_data(&source.failed_judgment_test);
        self.feature_layer_id.add_missing_data(&source.feature_layer_id);
        self.first.add_missing_data(&source.first);
        self.fled_civ_id.add_missing_data(&source.fled_civ_id);
        self.fooled_hf_id.add_missing_data(&source.fooled_hfid);
        self.form_id.add_missing_data(&source.form_id);
        self.framer_hf_id.add_missing_data(&source.framer_hfid);
        self.from_original.add_missing_data(&source.from_original);

        self.gambler_hf_id.add_missing_data(&source.gambler_hfid);
        self.ghost.add_missing_data(&source.ghost);
        self.giver_entity_id.add_missing_data(&source.giver_entity_id);
        self.giver_hf_id.add_missing_data(&source.giver_hist_figure_id);
        self.group_1_hf_id.add_missing_data(&source.group_1_hfid);
        self.group_2_hf_id.add_missing_data(&source.group_2_hfid);
        self.group_hf_id.add_missing_data(&source.group_hfid);

        self.hardship.add_missing_data(&source.hardship);
        self.held_firm_in_interrogation.add_missing_data(&source.held_firm_in_interrogation);
        self.hf_rep_1_of_2.add_missing_data(&source.hf_rep_1_of_2);
        self.hf_rep_2_of_1.add_missing_data(&source.hf_rep_2_of_1);
        self.hf_id.add_missing_data(&source.hfid);
        self.hf_id1.add_missing_data(&source.hfid1);
        self.hf_id2.add_missing_data(&source.hfid2);
        self.hf_id_target.add_missing_data(&source.hfid_target);
        self.hf_id.add_missing_data(&source.hist_fig_id);
        self.hf_id.add_missing_data(&source.hist_figure_id);
        self.honor_id.add_missing_data(&source.honor_id);

        self.identity_id.add_missing_data(&source.identity_id);
        self.identity_id1.add_missing_data(&source.identity_id1);
        self.identity_id2.add_missing_data(&source.identity_id2);
        self.implicated_hf_id.add_missing_data(&source.implicated_hfid);
        self.inherited.add_missing_data(&source.inherited);
        self.initiating_en_id.add_missing_data(&source.initiating_enid);
        self.instigator_hf_id.add_missing_data(&source.instigator_hfid);
        // TODO convert from String to i32
        // TODO check if this assignment works?
        // if let Some(interaction) = &source.interaction{
        //     println!("Unknown type: {:?}", interaction);
        // }
        // self.interaction_string.add_missing_data(&source.interaction);
        self.interrogator_hf_id.add_missing_data(&source.interrogator_hfid);

        self.join_entity_id.add_missing_data(&source.join_entity_id);
        self.joined_entity_id.add_missing_data(&source.joined_entity_id);
        self.joiner_entity_id.add_missing_data(&source.joiner_entity_id);
        self.joining_en_id.add_missing_data(&source.joining_enid);

        self.knowledge.add_missing_data(&source.knowledge);
        self.last_owner_hf_id.add_missing_data(&source.last_owner_hfid);
        self.law_add.add_missing_data(&source.law_add);
        self.law_remove.add_missing_data(&source.law_remove);
        self.leader_hf_id.add_missing_data(&source.leader_hfid);
        self.leaver_civ_id.add_missing_data(&source.leaver_civ_id);
        self.link.add_missing_data(&source.link);
        self.lost_value.add_missing_data(&source.lost_value);
        self.lure_hf_id.add_missing_data(&source.lure_hfid);

        self.master_wc_id.add_missing_data(&source.master_wcid);
        self.method.add_missing_data(&source.method);
        self.modification.add_missing_data(&source.modification);
        self.modifier_hf_id.add_missing_data(&source.modifier_hfid);
        self.mood.add_missing_data(&source.mood);
        self.moved_to_site_id.add_missing_data(&source.moved_to_site_id);

        self.name_only.add_missing_data(&source.name_only);
        self.new_ab_id.add_missing_data(&source.new_ab_id);
        self.new_account.add_missing_data(&source.new_account);
        self.new_artifact_id.add_missing_data(&source.new_artifact_id);
        if let Some(new_caste) = &source.new_caste {
            let new_caste = Some(caste_string_to_int(new_caste.clone()));
            self.new_caste.add_missing_data(&new_caste);
        }
        self.new_equipment_level.add_missing_data(&source.new_equipment_level);
        self.new_leader_hf_id.add_missing_data(&source.new_leader_hfid);
        self.new_race_id.add_missing_data(&source.new_race);
        self.new_site_civ_id.add_missing_data(&source.new_site_civ_id);
        self.no_defeat_mention.add_missing_data(&source.no_defeat_mention);
        self.no_prison_available.add_missing_data(&source.no_prison_available);

        self.occasion_id.add_missing_data(&source.occasion_id);
        self.old_ab_id.add_missing_data(&source.old_ab_id);
        self.old_account.add_missing_data(&source.old_account);
        self.old_artifact_id.add_missing_data(&source.old_artifact_id);
        if let Some(old_caste) = &source.old_caste {
            let old_caste = Some(caste_string_to_int(old_caste.clone()));
            self.old_caste.add_missing_data(&old_caste);
        }
        self.old_race_id.add_missing_data(&source.old_race);
        self.outcome.add_missing_data(&source.outcome);
        self.overthrown_hf_id.add_missing_data(&source.overthrown_hfid);

        self.partial_incorporation.add_missing_data(&source.partial_incorporation);
        self.payer_entity_id.add_missing_data(&source.payer_entity_id);
        self.payer_hf_id.add_missing_data(&source.payer_hfid);
        self.persecutor_en_id.add_missing_data(&source.persecutor_enid);
        self.persecutor_hf_id.add_missing_data(&source.persecutor_hfid);
        self.plotter_hf_id.add_missing_data(&source.plotter_hfid);
        self.pop_fl_id.add_missing_data(&source.pop_flid);
        self.pop_number_moved.add_missing_data(&source.pop_number_moved);
        self.pop_race.add_missing_data(&source.pop_race);
        self.pop_sr_id.add_missing_data(&source.pop_srid);
        self.pos_taker_hf_id.add_missing_data(&source.pos_taker_hfid);
        self.position_id.add_missing_data(&source.position_id);
        self.position_profile_id.add_missing_data(&source.position_profile_id);
        self.prison_months.add_missing_data(&source.prison_months);
        self.production_zone_id.add_missing_data(&source.production_zone_id);
        self.promise_to_hf_id.add_missing_data(&source.promise_to_hfid);
        self.property_confiscated_from_hf_id.add_missing_data(&source.property_confiscated_from_hfid);
        self.purchased_unowned.add_missing_data(&source.purchased_unowned);

        self.quality.add_missing_data(&source.quality);
        self.rampage_civ_id.add_missing_data(&source.rampage_civ_id);
        self.ransomed_hf_id.add_missing_data(&source.ransomed_hfid);
        self.ransomer_hf_id.add_missing_data(&source.ransomer_hfid);
        // TODO convert from String to i32
        // self.reason.add_missing_data(&source.reason);
        self.reason_id.add_missing_data(&source.reason_id);
        self.rebuild.add_missing_data(&source.rebuilt);
        self.rebuilt_ruined.add_missing_data(&source.rebuilt_ruined);
        self.receiver_entity_id.add_missing_data(&source.receiver_entity_id);
        self.receiver_hf_id.add_missing_data(&source.receiver_hist_figure_id);
        self.relationship.add_missing_data(&source.relationship);
        self.relevant_entity_id.add_missing_data(&source.relevant_entity_id);
        self.relevant_id_for_method.add_missing_data(&source.relevant_id_for_method);
        self.relevant_position_profile_id.add_missing_data(&source.relevant_position_profile_id);
        self.religion_id.add_missing_data(&source.religion_id);
        self.resident_civ_id.add_missing_data(&source.resident_civ_id);
        self.result.add_missing_data(&source.result);
        self.return_.add_missing_data(&source.return_);

        self.saboteur_hf_id.add_missing_data(&source.saboteur_hfid);
        self.schedule_id.add_missing_data(&source.schedule_id);
        self.searcher_civ_id.add_missing_data(&source.searcher_civ_id);
        self.season.add_missing_data(&source.season);
        self.secret_goal.add_missing_data(&source.secret_goal);
        self.seeker_hf_id.add_missing_data(&source.seeker_hfid);
        self.seller_hf_id.add_missing_data(&source.seller_hfid);
        self.shrine_amount_destroyed.add_missing_data(&source.shrine_amount_destroyed);
        self.site_civ_id.add_missing_data(&source.site_civ_id);
        self.site_entity_id.add_missing_data(&source.site_entity_id);
        self.site_hf_id.add_missing_data(&source.site_hfid);
        self.site_id.add_missing_data(&source.site_id);
        self.site_id_1.add_missing_data(&source.site_id1);
        self.site_id_2.add_missing_data(&source.site_id2);
        self.site_id_1.add_missing_data(&source.site_id_1);
        self.site_id_2.add_missing_data(&source.site_id_2);
        self.site_property_id.add_missing_data(&source.site_property_id);
        self.situation.add_missing_data(&source.situation);
        self.skill_at_time.add_missing_data(&source.skill_at_time);
        if let Some(slayer_caste) = &source.slayer_caste {
            let slayer_caste = Some(caste_string_to_int(slayer_caste.clone()));
            self.slayer_caste.add_missing_data(&slayer_caste);
        }
        self.slayer_hf_id.add_missing_data(&source.slayer_hfid);
        self.slayer_item_id.add_missing_data(&source.slayer_item_id);
        self.slayer_race.add_missing_data(&source.slayer_race);
        self.slayer_shooter_item_id.add_missing_data(&source.slayer_shooter_item_id);
        self.snatcher_hf_id.add_missing_data(&source.snatcher_hfid);
        self.source_entity_id.add_missing_data(&source.source_entity_id);
        self.source_site_id.add_missing_data(&source.source_site_id);
        self.source_structure_id.add_missing_data(&source.source_structure_id);
        self.speaker_hf_id.add_missing_data(&source.speaker_hfid);
        self.spotter_hf_id.add_missing_data(&source.spotter_hfid);
        self.start.add_missing_data(&source.start);
        self.state.add_missing_data(&source.state);
        self.structure_id.add_missing_data(&source.structure_id);
        self.student_hf_id.add_missing_data(&source.student_hfid);
        self.region_id.add_missing_data(&source.subregion_id);
        self.subtype.add_missing_data(&source.subtype);
        self.successful.add_missing_data(&source.successful);
        self.surveiled_coconspirator.add_missing_data(&source.surveiled_coconspirator);
        self.surveiled_contact.add_missing_data(&source.surveiled_contact);
        self.surveiled_convicted.add_missing_data(&source.surveiled_convicted);
        self.surveiled_target.add_missing_data(&source.surveiled_target);

        self.target_civ_id.add_missing_data(&source.target_civ_id);
        self.target_en_id.add_missing_data(&source.target_enid);
        self.target_hf_id.add_missing_data(&source.target_hfid);
        self.target_identity.add_missing_data(&source.target_identity);
        self.target_seen_as.add_missing_data(&source.target_seen_as);
        self.teacher_hf_id.add_missing_data(&source.teacher_hfid);
        self.took_items.add_missing_data(&source.took_items);
        self.took_livestock.add_missing_data(&source.took_livestock);
        self.top_facet.add_missing_data(&source.top_facet);
        self.top_facet_modifier.add_missing_data(&source.top_facet_modifier);
        self.top_facet_rating.add_missing_data(&source.top_facet_rating);
        self.top_relationship_factor.add_missing_data(&source.top_relationship_factor);
        self.top_relationship_modifier.add_missing_data(&source.top_relationship_modifier);
        self.top_relationship_rating.add_missing_data(&source.top_relationship_rating);
        self.top_value.add_missing_data(&source.top_value);
        self.top_value_modifier.add_missing_data(&source.top_value_modifier);
        self.top_value_rating.add_missing_data(&source.top_value_rating);
        self.topic.add_missing_data(&source.topic);
        self.trader_entity_id.add_missing_data(&source.trader_entity_id);
        self.trader_hf_id.add_missing_data(&source.trader_hfid);
        self.trickster_hf_id.add_missing_data(&source.trickster_hfid);

        self.unit_id.add_missing_data(&source.unit_id);
        self.unit_type.add_missing_data(&source.unit_type);
        self.unretire.add_missing_data(&source.unretire);

        self.wanted_and_recognized.add_missing_data(&source.wanted_and_recognized);
        self.was_raid.add_missing_data(&source.was_raid);
        self.was_torture.add_missing_data(&source.was_torture);
        self.wc_id.add_missing_data(&source.wc_id);
        self.wc_id.add_missing_data(&source.wcid);
        self.winner_hf_id.add_missing_data(&source.winner_hfid);
        self.woundee_hf_id.add_missing_data(&source.woundee_hfid);
        self.wounder_hf_id.add_missing_data(&source.wounder_hfid);
        self.wrongful_conviction.add_missing_data(&source.wrongful_conviction);
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEvent {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::HistoricalEvent>, HistoricalEvents>
    for Vec<df_st_core::HistoricalEvent>
{
    fn add_missing_data(&mut self, source: &HistoricalEvents) {
        self.add_missing_data(&source.historical_event);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalEvent>, HistoricalEvents>
    for IndexMap<u64, df_st_core::HistoricalEvent>
{
    fn add_missing_data(&mut self, source: &HistoricalEvents) {
        self.add_missing_data(&source.historical_event);
    }
}

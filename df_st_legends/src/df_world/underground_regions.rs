use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown, HashAndPartialEqById,
)]
pub struct UndergroundRegion {
    pub id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub depth: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown)]
pub struct UndergroundRegions {
    pub underground_region: Option<Vec<UndergroundRegion>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::UndergroundRegion, UndergroundRegion> for df_st_core::UndergroundRegion {
    fn add_missing_data(&mut self, source: &UndergroundRegion) {
        self.id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.depth.add_missing_data(&source.depth);
    }
}

impl PartialEq<df_st_core::UndergroundRegion> for UndergroundRegion {
    fn eq(&self, other: &df_st_core::UndergroundRegion) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::UndergroundRegion>, UndergroundRegions>
    for Vec<df_st_core::UndergroundRegion>
{
    fn add_missing_data(&mut self, source: &UndergroundRegions) {
        self.add_missing_data(&source.underground_region);
    }
}

impl Filler<IndexMap<u64, df_st_core::UndergroundRegion>, UndergroundRegions>
    for IndexMap<u64, df_st_core::UndergroundRegion>
{
    fn add_missing_data(&mut self, source: &UndergroundRegions) {
        self.add_missing_data(&source.underground_region);
    }
}

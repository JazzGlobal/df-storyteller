use df_st_core::{
    ConvertingUtils, DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown,
};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct Artifact {
    pub id: i32,
    pub name: Option<String>,
    pub item: Option<Item>,
    pub site_id: Option<i32>,
    pub structure_local_id: Option<i32>,
    pub holder_hfid: Option<i32>,

    pub abs_tile_x: Option<i32>,
    pub abs_tile_y: Option<i32>,
    pub abs_tile_z: Option<i32>,
    pub subregion_id: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct Artifacts {
    pub artifact: Option<Vec<Artifact>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct Item {
    pub name_string: Option<String>,
    pub page_number: Option<i32>,
    pub page_written_content_id: Option<i32>,
    pub writing_written_content_id: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Artifact, Artifact> for df_st_core::Artifact {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &Artifact) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.site_id.add_missing_data(&source.site_id.negative_to_none());
        self.structure_local_id.add_missing_data(&source.structure_local_id.negative_to_none());
        self.holder_hf_id.add_missing_data(&source.holder_hfid.negative_to_none());
        self.abs_tile_x.add_missing_data(&source.abs_tile_x);
        self.abs_tile_y.add_missing_data(&source.abs_tile_y);
        self.abs_tile_z.add_missing_data(&source.abs_tile_z);
        self.region_id.add_missing_data(&source.subregion_id.negative_to_none());

        if let Some(item) = &source.item {
            self.item_name.add_missing_data(&item.name_string);
            self.item_page_number.add_missing_data(&item.page_number);
            self.item_page_written_content_id.add_missing_data(&item.page_written_content_id.negative_to_none());
            self.item_writing_written_content_id.add_missing_data(&item.writing_written_content_id.negative_to_none());
        }
    }
}

impl PartialEq<df_st_core::Artifact> for Artifact {
    fn eq(&self, other: &df_st_core::Artifact) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::Artifact>, Artifacts> for Vec<df_st_core::Artifact> {
    fn add_missing_data(&mut self, source: &Artifacts) {
        self.add_missing_data(&source.artifact);
    }
}

impl Filler<IndexMap<u64, df_st_core::Artifact>, Artifacts>
    for IndexMap<u64, df_st_core::Artifact>
{
    fn add_missing_data(&mut self, source: &Artifacts) {
        self.add_missing_data(&source.artifact);
    }
}

use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct DanceForm {
    pub id: i32,
    pub description: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct DanceForms {
    pub dance_form: Option<Vec<DanceForm>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::DanceForm, DanceForm> for df_st_core::DanceForm {
    fn add_missing_data(&mut self, source: &DanceForm) {
        self.id.add_missing_data(&source.id);
        self.description.add_missing_data(&source.description);
    }
}

impl PartialEq<df_st_core::DanceForm> for DanceForm {
    fn eq(&self, other: &df_st_core::DanceForm) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::DanceForm>, DanceForms> for Vec<df_st_core::DanceForm> {
    fn add_missing_data(&mut self, source: &DanceForms) {
        self.add_missing_data(&source.dance_form);
    }
}

impl Filler<IndexMap<u64, df_st_core::DanceForm>, DanceForms>
    for IndexMap<u64, df_st_core::DanceForm>
{
    fn add_missing_data(&mut self, source: &DanceForms) {
        self.add_missing_data(&source.dance_form);
    }
}
